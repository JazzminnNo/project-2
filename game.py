import pygame
import sys
import os
import settings
import inputs
import color
import font
import menu
import menu_screen
import highscore
import sound
import assets
import random
import math
import minigames.minigame1.minigame1 as minigame1
import minigames.minigame2.minigame2 as minigame2
import minigames.minigame3.minigame3 as minigame3
import minigames.minigame4.minigame4 as minigame4
import minigames.minigame5.minigame5 as minigame5
import minigames.minigame6.minigame6 as minigame6


os.environ['SDL_VIDEO_CENTERED'] = '1'

pygame.init()
pygame.mixer.init()
pygame.display.set_caption('Blobolympics')

settings = settings.Settings(os)
inputs = inputs.Inputs()
screen = pygame.display.set_mode((settings.width, settings.height))
clock = pygame.time.Clock()

color = color.Color()
font = font.Font(pygame)
highscore = highscore.Highscores(os)
sound = sound.Sound(os, pygame)
assets = assets.Assets(pygame, settings)
menuScreen = menu_screen.MenuScreen(pygame, settings, assets, color)
menu = menu.Menu(font=font, color=color, settings=settings)

icon = assets.logo
pygame.display.set_icon(icon)
pygame.mixer.music.load(os.path.join(sound.snd_dir, 'ponpon.mp3'))

music_play = True

minigame1 = minigame1.Minigame1(pygame, settings, highscore, sound)
minigame2 = minigame2.Minigame2(pygame, screen, settings, assets, inputs, color, random, highscore, sound)
minigame3 = minigame3.Minigame3(pygame, settings, color, screen, assets, inputs, highscore, sound)
minigame4 = minigame4.Minigame4(pygame, settings, screen, color, inputs, random, math, highscore, sound)
minigame5 = minigame5.Minigame5(pygame, settings, inputs, color, screen, random, highscore, assets, sound)
minigame6 = minigame6.Minigame6(pygame, settings, highscore, sound)


minigames = [minigame1, minigame2, minigame3, minigame4, minigame5, minigame6]


def update():
    global music_play
    if music_play:
        pygame.mixer.music.set_volume(0.2)
        pygame.mixer.music.play(loops=-1)
        music_play = False

    menuScreen.update_blobs(settings=settings)

    inputs.update_mouse(pygame.mouse.get_pressed(), pygame.mouse.get_pos())
    inputs.add_events(pygame.event.get(), pygame)

    if inputs.isset(pygame.QUIT):
        settings.running = False

    if inputs.down(pygame.K_F6):
        minigames[settings.current_minigame].start(settings=settings, assets=assets, pygame=pygame, random=random, color=color, font=font)

    if inputs.down(pygame.K_F7):
        settings.debug = not settings.debug

    menu.update(pygame=pygame, inputs=inputs, settings=settings, minigames=minigames, assets=assets, random=random, color=color, font=font)

    if settings.current_minigame is not None and not settings.paused:
        minigames[settings.current_minigame].update(pygame=pygame, inputs=inputs, settings=settings, screen=screen, color=color, random=random, math=math, highscore=highscore, clock=clock, assets=assets, menu=menu)

    inputs.update(pygame)


def draw():
    menuScreen.draw_screen(settings=settings, screen=screen, assets=assets)

    if settings.current_minigame is not None:
        minigames[settings.current_minigame].draw(screen=screen, settings=settings, assets=assets, pygame=pygame, color=color, highscore=highscore)


    menu.draw(pygame=pygame, color=color, screen=screen, settings=settings, font=font, highscore=highscore)

    if settings.debug:
        screen.blit(font.fps.render(str(int(clock.get_fps())), True, color.white), (10, 10))

    pygame.display.update()


while settings.running:
    update()
    draw()
    settings.tick = clock.tick(settings.fps)


pygame.quit()
sys.exit()
