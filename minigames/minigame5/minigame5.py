class Minigame5:

    def __init__(self, pygame, settings, inputs, color, screen, random, highscore, assets, sound):
        self.bkgd = pygame.image.load(settings.script_dir + "/minigames/minigame5/assets/SkiPiste_with_texture.png").convert()
        self.RedFlagImg = pygame.image.load(
            settings.script_dir + "/minigames/minigame5/assets/redflag3.png").convert_alpha()
        self.BlueFlagImg = pygame.image.load(
            settings.script_dir + "/minigames/minigame5/assets/blueflag.png").convert_alpha()
        self.RockImg = pygame.image.load(
            settings.script_dir + "/minigames/minigame5/assets/snowstone.png").convert_alpha()
        self.FinishLineImg = pygame.image.load(
            settings.script_dir + "/minigames/minigame5/assets/Finishline_definitief.png").convert_alpha()

        self.redflag_rect = self.RedFlagImg.get_rect()
        self.blueflag_rect = self.BlueFlagImg.get_rect()
        self.snowstone1_rect = self.RockImg.get_rect()
        self.snowstone2_rect = self.RockImg.get_rect()

        self.leftBoundary = 165
        self.rightBoundary = 1034

        self.leftkeyImg = assets.key_left
        self.leftkeyImg = pygame.transform.scale(self.leftkeyImg, (75, 75))
        self.rightkeyImg = assets.key_right
        self.rightkeyImg = pygame.transform.scale(self.rightkeyImg, (75, 75))
        self.spacebarImg = assets.spacebar

        self.screenWidth = settings.width
        self.hs = highscore

        self.gamefont = pygame.font.Font(None, 30)
        self.settings = settings
        self.screen = screen
        self.color = color
        self.pygame = pygame
        self.random = random
        self.inputs = inputs
        self.assets = assets
        self.sound = sound
        self.missedFlags = 0

        self.fps = self.settings.fps

        self.font_name = pygame.font.match_font('verdana')
        self.font = pygame.font.Font(self.font_name, 35)
        self.fontBigger = self.pygame.font.Font(self.font_name, 100)

        self.crowd_response_play = True


    class Player:

        def __init__(self, x, y, width, height, image, sound):
            self.x = x
            self.y = y
            self.width = width
            self.height = height
            self.image = image
            self.speed = 0
            self.player_rect = self.image.get_rect()

            self.flags_passed = 0
            self.score = 0
            self.has_incremented = False

            self.accelerationRate = 0.1
            self.decelerationRate = 0.1

            self.positionChangeRate = 10
            self.minimumSpeed = 0
            self.maximumSpeed = 12

            self.sound = sound

        def modify_position(self, pygame, input):
            if input.isset(pygame.K_RIGHT):
                self.x += self.positionChangeRate
                #self.sound.ski_sound.play()
            if input.isset(pygame.K_LEFT):
                self.x -= self.positionChangeRate
                #self.sound.ski_sound.play()

        def modify_speed(self, pygame, input):
            if input.isset(pygame.K_SPACE):
                self.speed += self.accelerationRate
            else:
                self.speed -= self.decelerationRate

            if self.speed > self.maximumSpeed:
                self.speed = self.maximumSpeed

            if self.speed < self.minimumSpeed:
                self.speed = self.minimumSpeed

        def has_collided(self, flag, player, max_flags):
            # print('from has_collided:', player.flags_passed)
            player.player_rect.x = player.x
            player.player_rect.y = player.y

            if player.flags_passed != max_flags - 1:

                # if player.x < flag.x < player.x + player.width:
                #   if player.y >= flag.y:

                if player.has_incremented is False:
                    if player.player_rect.colliderect(flag.flag_rect):
                            #print('its a hit')
                            player.score += 1
                            player.has_incremented = True
                            self.sound.jeej.play()

                if player.y > flag.y + flag.height:
                    player.has_incremented = False
                    player.flags_passed += 1
                    if player.flags_passed >= max_flags - 1:
                        player.flags_passed = max_flags - 1

        def boundaries(self, leftbound, rightbound):
            if self.x > rightbound:
                self.x = rightbound
            if self.x < leftbound:
                self.x = leftbound

        def update(self, pygame, input, leftbound, rightbound):
            self.modify_position(pygame, input)
            self.modify_speed(pygame, input)
            self.boundaries(leftbound, rightbound)

        def draw(self, screen):
            screen.blit(self.image, (self.x, self.y))

    class Flag:

        def __init__(self, x, y, width, height, image):
            self.x = x
            self.y = y
            self.width = width
            self.height = height
            self.image = image
            self.flag_rect = self.image.get_rect()


        def draw(self, screen):
            screen.blit(self.image, (self.x, self.y))

    class Finish:

        def __init__(self, x, y, img):
                self.x = x
                self.y = y
                self.img = img

        def is_finished(self, player):
                if self.y < player.y:
                    return True
                return False

        def update(self, player):
            if self.is_finished(player):
                self.y = self.y - 10
            else:
                self.y = self.y - player.speed
                self.is_finished(player)

        def draw(self, screen):
                screen.blit(self.img, (self.x, self.y))

    def setup_flags(self):

        if self.difficulty == 0:
            self.amountOfFlags = 10
            flago = []
            for i in range(0, self.amountOfFlags):  # Currently 25
                flago.append(self.Flag((self.random.randint(165, 1035)), (i * 1800 + 2000), self.RedFlagImg.get_rect().width, self.RedFlagImg.get_rect().height, self.RedFlagImg))
            return flago

        elif self.difficulty == 1:
            self.amountOfFlags = 15
            flago = []
            for i in range(0, self.amountOfFlags):  # Currently 25
                flago.append(self.Flag((self.random.randint(165, 1035)), (i * 1200 + 2000), self.RedFlagImg.get_rect().width, self.RedFlagImg.get_rect().height, self.RedFlagImg))
            return flago

        else:
            flago = []
            self.amountOfFlags = 20
            for i in range(0, self.amountOfFlags):  # Currently 25
                flago.append(self.Flag((self.random.randint(165, 1035)), (i * 600 + 2000), self.RedFlagImg.get_rect().width, self.RedFlagImg.get_rect().height, self.RedFlagImg))
            return flago

    def draw_flags(self, screen, all_flags, player, max_flags):
        for i in range(len(all_flags)):
            all_flags[i].draw(screen)
            #print('from draw_flags:', player.flags_passed)
            all_flags[player.flags_passed].flag_rect.x = all_flags[i].x
            all_flags[player.flags_passed].flag_rect.y = all_flags[i].y
            #print(all_flags[player.flags_passed].flag_rect)
            self.player.has_collided(all_flags[player.flags_passed], player, max_flags)
            all_flags[i].y -= player.speed

    #def update_draw_flags(self, all_flags):
        #for i in range(len(all_flags)):
            #all_flags[i].y -= 6

    # Creates and returns a list of two elements: RedFlagImg and BlueFlagImg
    def random_flags(self, RedFlagImg, BlueFlagImg):
        flags_list = []
        for x in range(0, 1):
            flags_list.append(RedFlagImg)
            flags_list.append(BlueFlagImg)
        return flags_list

    def draw_lined_rect(self, pygame, screen, x, y, width, height, rect_color, line_color, line_thickness=2):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.rect_color = rect_color
        self.line_color = line_color
        self.line_thickness = line_thickness

        pygame.draw.rect(screen, self.rect_color, (self.x, self.y, self.width, self.height))
        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x + self.width, self.y), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y + self.height), (self.x + self.width, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x + self.width, self.y), (self.x + self.width, self.y + self.height), self.line_thickness)

    def draw_hud(self, player):

        # TIME CONTAINER
        self.draw_lined_rect(self.pygame, self.screen, 965, 5, 295, 100, self.color.light_grey, self.color.black)
        # TIME
        self.draw_lined_rect(self.pygame, self.screen, 980, 15, 110, 80, self.color.black, self.color.black)
        # TIMER
        self.draw_lined_rect(self.pygame, self.screen, 1100, 15, 140, 80, self.color.dark_gold, self.color.black)

        # TIME TEXT
        self.draw_text(self.screen, 'TIME', 19, 1035, 20, self.color.white, self.pygame)

        # FLAG TEXT
        self.draw_text(self.screen, 'FLAGS', 19, 1035, 48, self.color.white, self.pygame)

        # MISSED FLAG TEXT
        self.draw_text(self.screen, 'MISSED FLAGS', 13, 1035, 76, self.color.white, self.pygame)

        # KEYS CONTAINER
        self.draw_lined_rect(self.pygame, self.screen, 450, 600, 800, 80, self.color.light_grey, self.color.black)
        # MOVE
        self.draw_lined_rect(self.pygame, self.screen, 465, 610, 120, 60, self.color.robrecht_blue, self.color.black)
        # MOVE TEXT
        self.draw_text(self.screen, 'Move:', 30, 525, 620, self.color.black, self.pygame)

        # SPEED CONTAINER
        self.draw_lined_rect(self.pygame, self.screen, 20, 600, 350, 80, self.color.light_grey, self.color.black)

        # SPEED
        self.draw_lined_rect(self.pygame, self.screen, 35, 610, 120, 60, self.color.green, self.color.black)

        # SPEED BAR CONTAINER
        self.draw_lined_rect(self.pygame, self.screen, 170, 610, 180, 60, self.color.dim_gray, self.color.black)


        # SPEED BAR
        if self.difficulty == 0:
            if player.speed > 0:
                if player.speed < 6:
                    self.draw_lined_rect(self.pygame, self.screen, 170, 610, player.speed * 15, 60, self.color.green, self.color.black)
                elif player.speed > 6:
                    self.draw_lined_rect(self.pygame, self.screen, 170, 610, player.speed * 15, 60, self.color.red, self.color.black)
                else:
                    self.draw_lined_rect(self.pygame, self.screen, 170, 610, player.speed * 15, 60, self.color.orange, self.color.black)

        if self.difficulty == 1:
            if player.speed > 0:
                if player.speed < 6:
                    self.draw_lined_rect(self.pygame, self.screen, 170, 610, player.speed * 15, 60, self.color.green, self.color.black)
                elif player.speed > 6:
                    self.draw_lined_rect(self.pygame, self.screen, 170, 610, player.speed * 15, 60, self.color.red, self.color.black)
                else:
                    self.draw_lined_rect(self.pygame, self.screen, 170, 610, player.speed * 15, 60, self.color.orange, self.color.black)

        if self.difficulty == 2:
            if player.speed > 0:
                if player.speed < 6:
                    self.draw_lined_rect(self.pygame, self.screen, 170, 610, player.speed * 15, 60, self.color.green, self.color.black)
                elif player.speed > 6:
                    self.draw_lined_rect(self.pygame, self.screen, 170, 610, player.speed * 15, 60, self.color.red, self.color.black)
                else:
                    self.draw_lined_rect(self.pygame, self.screen, 170, 610, player.speed * 15, 60, self.color.orange, self.color.black)

        # SPEED BAR BORDERS 1
        self.draw_lined_rect(self.pygame, self.screen, 170, 610, 180, 10, self.color.black, self.color.black)
        # SPEED BAR BORDERS 2
        self.draw_lined_rect(self.pygame, self.screen, 170, 660, 180, 10, self.color.black, self.color.black)

        # JUMP
        self.draw_lined_rect(self.pygame, self.screen, 800, 610, 180, 60, self.color.robrecht_blue, self.color.black)

        # SPEED UP BUTTON
        self.draw_lined_rect(self.pygame, self.screen, 1000, 610, 240, 60, self.color.grey, self.inputSpaceKeyBorderColor, 6)
        self.screen.blit(self.spacebarImg, (1000, 610))

        # SPEED UP BUTTON
        self.draw_text(self.screen, 'Speed up:', 30, 890, 620, self.color.black, self.pygame)

        # SPEED TEXT
        self.draw_text(self.screen, 'SPEED', 30, 95, 620, self.color.black, self.pygame)

        # LEFT ARROW
        self.draw_lined_rect(self.pygame, self.screen, 612, 605, 70, 70, self.color.grey, self.inputLeftKeyBorderColor, 6)
        self.screen.blit(self.leftkeyImg, (610, 605))

        # RIGHT ARROW
        self.draw_lined_rect(self.pygame, self.screen, 700, 605, 70, 70, self.color.grey, self.inputRightKeyBorderColor, 6)
        self.screen.blit(self.rightkeyImg, (700, 605))

    def show_countdown(self):
        text = self.fontBigger.render(str(self.total_sec), True, (0, 0, 0))
        self.screen.blit(text, (self.settings.width / 2, 60))

    def convert_hs_to_time(self):
        totale_sec = self.highscore // self.fps
        minuten = totale_sec // 60
        seconden = totale_sec % 60
        frame_per_seconde = self.highscore % 60
        string = "{0:02}:{1:02}:{2:02}".format(minuten, seconden, frame_per_seconde)
        return string

    def show_timer(self):
        totale_sec = self.fps_count // self.fps
        minuten = totale_sec // 60
        seconden = totale_sec % 60
        frame_per_seconde = self.fps_count % 60
        string = "{0:02}:{1:02}:{2:02}".format(minuten, seconden, frame_per_seconde)
        self.draw_text(self.screen, string, 19, 1170, 20, self.color.black, self.pygame)

    def show_start_screen(self, screen, settings, pygame):
        image = self.pygame.Surface([settings.width, settings.height]).convert()
        image.set_alpha(200)  # , pygame.SRCALPHA, 32)
        image.fill((0, 0, 0))
        rect = image.get_rect()
        rect.center = (settings.width / 2, (settings.height / 2))
        screen.blit(image, rect)
        self.draw_text(screen, 'Skiing!', 64, settings.width / 2, settings.height / 4, self.color.white, pygame)  # game splash/start screen
        self.draw_text(screen, 'Press Space to start the game', 22, settings.width / 2, settings.height / 2, self.color.white, pygame)  # game splash/start screen
        self.draw_text(screen, 'Highscore is: ' + self.convert_hs_to_time(), 22, settings.width / 2, settings.height / 2 + 60, self.color.white, pygame)

    def show_flagcounter(self):
        flag_score = str(self.player.score) + "/" + str(self.amountOfFlags)
        self.draw_text(self.screen, flag_score, 19, 1170, 47, self.color.black, self.pygame)

    def show_missed_flags(self):
        missed_flags = str(self.missedFlags)
        self.draw_text(self.screen, missed_flags, 19, 1170, 72, self.color.black, self.pygame)


    def return_score(self, has_finished, missed_flags):
        if has_finished:
            return ((self.fps_count // self.fps) % 60) + missed_flags, self.fps_count % 60



    def draw_text(self, surf, text, size, x, y, color, pygame):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)  # lettertype en lettergrootte
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        surf.blit(text_surface, text_rect)

    def start(self, **kwargs):
        if 'selected_button' in kwargs:
            self.difficulty = kwargs['selected_button']

        self.amountOfFlags = 25
        self.z = 0
        self.counter = 0
        self.fps_count = 60
        self.countdown_done = False
        self.countdown = 3
        self.total_sec = 0
        self.fps_count_resetToZero = False

        self.playerImg = self.assets.henkblob
        self.playerImg = self.pygame.transform.scale(self.playerImg, (90, 140))
        self.player_rect = self.playerImg.get_rect()

        self.player = self.Player(600, 100, self.playerImg.get_rect().width, self.playerImg.get_rect().height, self.playerImg, self.sound)

        self.all_flags = self.setup_flags()

        if self.difficulty == 0:
            self.finish = self.Finish(165, 19000, self.FinishLineImg)
        if self.difficulty == 1:
            self.finish = self.Finish(165, 19000, self.FinishLineImg)
        if self.difficulty == 2:
            self.finish = self.Finish(165, 14000, self.FinishLineImg)


        self.playerFinished = False
        self.endTime = 0
        self.endTimeString = ''
        self.countedflags = False
        self.fps_count = 0

        self.missedFlags = 0
        self.missedFlagsOld = self.missedFlags

        self.inputKeysDefaultColor = self.color.grey
        self.inputLeftKeyBorderColor = self.inputKeysDefaultColor
        self.inputRightKeyBorderColor = self.inputKeysDefaultColor
        self.inputSpaceKeyBorderColor = self.inputKeysDefaultColor

        self.highscore = self.hs.load_highscore_skiing(self.difficulty)
        self.started =  False

    def update(self, **kwargs):

        if self.difficulty == 0:
            self.player.accelerationRate = 0.1
            self.player.decelerationRate = 0.1

            self.player.positionChangeRate = 8
            self.player.minimumSpeed = 0
            self.player.maximumSpeed = 12

        if self.difficulty == 0:
            self.player.accelerationRate = 0.1
            self.player.decelerationRate = 0.1

            self.player.positionChangeRate = 8
            self.player.minimumSpeed = 0
            self.player.maximumSpeed = 12

        if self.difficulty == 2:
            self.player.accelerationRate = 0.1
            self.player.decelerationRate = 0.1

            self.player.positionChangeRate = 8
            self.player.minimumSpeed = 0
            self.player.maximumSpeed = 12

        if self.inputs.down(self.pygame.K_RIGHT):
            self.inputRightKeyBorderColor = self.color.blue
        else:
            self.inputRightKeyBorderColor = self.inputKeysDefaultColor
        if self.inputs.down(self.pygame.K_LEFT):
            self.inputLeftKeyBorderColor = self.color.blue
        else:
            self.inputLeftKeyBorderColor = self.inputKeysDefaultColor
        if self.inputs.down(self.pygame.K_SPACE):
            self.started = True
        if self.inputs.down(self.pygame.K_SPACE):
            self.inputSpaceKeyBorderColor = self.color.blue
        else:
            self.inputSpaceKeyBorderColor = self.inputKeysDefaultColor

        if self.started:

            if self.missedFlags > self.missedFlagsOld:
                self.sound.ohoh.play()
                self.missedFlagsOld = self.missedFlags

            if self.countedflags is True:
                self.counter += 1

            if self.playerFinished is False:
                self.fps_count += 1
                self.total_sec = self.countdown - (self.fps_count // self.fps)

            self.finish.update(self.player)

            if self.countdown_done is True:
                if self.playerFinished is False:
                    self.player.update(self.pygame, self.inputs, self.leftBoundary, self.rightBoundary)

                    self.missedFlags = self.player.flags_passed - self.player.score
                #print('flags passed:', self.player.flags_passed)
                #print('score: ', self.player.score)
                #print('missed: ', self.missedFlags)

            if self.playerFinished is True:
                self.endTime = (self.return_score(self.playerFinished, self.missedFlags + 1))
                self.endTimeString = str(self.endTime[0]) + '.' + str(self.endTime[1]) + ' seconds'
                if self.fps_count < self.highscore:
                    kwargs['menu'].score_screen_show(**kwargs,
                                                     text='Time:   ' + self.endTimeString + "\n\nA new highscore is achieved! It's now:  " + self.endTimeString)
                else:
                    kwargs['menu'].score_screen_show(**kwargs,
                                                     text='Time:   ' + self.endTimeString + "\n\nSuch a shame! The highscore to beat is: " + self.convert_hs_to_time())


            self.playerFinished = self.finish.is_finished(self.player)

            self.z -= self.player.speed

    def draw(self, **kwargs):

        # print(self.has_incremented)
        rel_z = self.z % self.bkgd.get_rect().width
        kwargs['screen'].blit(self.bkgd, (0, rel_z - self.bkgd.get_rect().width))
        if rel_z < kwargs['settings'].width:
            kwargs['screen'].blit(self.bkgd, (0, rel_z))
        #self.z -= 6

        self.finish.draw(self.screen)
        self.player.draw(self.screen)

        if not self.playerFinished:
            self.draw_flags(self.screen, self.all_flags, self.player, self.amountOfFlags)

        if self.total_sec > 0 and not self.countdown_done:
            self.show_countdown()
        if self.total_sec == 0 and not self.countdown_done:
            self.draw_text(self.screen, "Start!", 100, self.settings.width / 2, 60, self.color.black, self.pygame)
        if -1 < self.total_sec < 0:
            self.total_sec = 0
        if not self.fps_count_resetToZero and self.total_sec == -1:
            self.fps_count = 0
            self.fps_count_resetToZero = True
            self.countdown_done = True
        if self.countdown_done and not self.playerFinished:
            self.draw_hud(self.player)
            self.show_timer()
            self.show_flagcounter()
            self.show_missed_flags()
        if self.playerFinished:
            if self.fps_count < self.highscore:
                self.hs.save_highscore(self.fps_count, 5, self.difficulty)
                if self.crowd_response_play is True:
                    self.sound.partyhorn.play()
                    self.sound.crowd_cheer_sound.play()
                    self.pygame.mixer.fadeout(18000)
                    self.crowd_response_play = False
            else:
                if self.crowd_response_play is True:
                    self.sound.cue_scratch.play()
                    self.sound.crowd_boo_sound.play()
                    self.pygame.mixer.fadeout(18000)
                    self.crowd_response_play = False

        if not self.started:
            self.show_start_screen(self.screen, self.settings, self.pygame)


        #print('player rect:', self.player_rect)
        #print('flag rect', self.all_flags.)
        #print('player x:', self.player.x)