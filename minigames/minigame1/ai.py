class AI:

    def __init__(self, **kwargs):
        self.difficulty = kwargs['difficulty']
        self.running = False
        self.aiming = False
        self.throw = False

        self.max_distance = 2250 + 50 * self.difficulty
        self.max_rotation = 15 + 5 * self.difficulty

        self.delay_time = 0
        self.delay = True

        self.score = 0

    def update(self, **kwargs):
        if self.delay:
            self.delay_time += kwargs['clock'].get_time()

            if self.delay_time > 1000:
                self.delay = False
                self.running = True

        if kwargs['player'].x >= self.max_distance + kwargs['background_x']:
            self.running = False
            self.throw = True

        if kwargs['player'].x >= 500 + kwargs['background_x']:
            self.aiming = True

        if kwargs['player'].spear.rotation >= self.max_rotation:
            self.aiming = False
