class Spear:

    def __init__(self, **kwargs):
        self.img = kwargs['spear']
        self.spear = self.img
        self.x = 120
        self.y = 340
        self.center = self.spear.get_rect().center
        self.rotation = 0
        self.rotation_change = 0.05
        self.holding = True
        self.mid_screen = False
        self.velocity = 20
        self.x_velocity = 0
        self.y_velocity = 0
        self.gravity = 0
        self.low_angle = False
        self.landed = False

    def update(self, **kwargs):
        if self.holding and ((kwargs['inputs'].isset(kwargs['pygame'].K_UP) or kwargs['inputs'].isset(kwargs['pygame'].K_DOWN) and kwargs['state'] is kwargs['gamestate'].PLAYER) or (kwargs['ai'].aiming and kwargs['state'] is kwargs['gamestate'].AI)):
            if ((kwargs['inputs'].isset(kwargs['pygame'].K_UP) and kwargs['state'] is kwargs['gamestate'].PLAYER) or (kwargs['ai'].aiming and kwargs['state'] is kwargs['gamestate'].AI)) and self.rotation < 50:
                self.rotation += 1
            elif (kwargs['inputs'].isset(kwargs['pygame'].K_DOWN) and kwargs['state'] is kwargs['gamestate'].PLAYER) and self.rotation > 0:
                self.rotation -= 1
            self.spear = kwargs['pygame'].transform.rotate(self.img, self.rotation)

        self.center = self.spear.get_rect().center

        if self.holding and ((kwargs['inputs'].down(kwargs['pygame'].K_SPACE) and kwargs['state'] is kwargs['gamestate'].PLAYER) or (kwargs['ai'].throw and kwargs['state'] is kwargs['gamestate'].AI)):
            self.holding = False
            self.velocity += kwargs['speed']

        if (self.y + self.spear.get_rect()[3] > kwargs['settings'].height / 1.5) and self.rotation < 0 and kwargs['bg_y'] == 0:
            self.landed = True

        if not self.holding and not self.landed:
            self.x_velocity = self.velocity * kwargs['math'].cos(kwargs['math'].radians(self.rotation))
            self.y_velocity = self.velocity * kwargs['math'].sin(kwargs['math'].radians(self.rotation))

            if self.rotation >= 0 and self.velocity > 0:
                self.velocity -= (0.01 * (self.rotation / 4))
            elif self.rotation < 0:
                self.velocity += 0.05
            else:
                self.velocity = 0

            if not self.mid_screen:
                self.x += self.x_velocity

            if not kwargs['camera_moving']:
                self.y -= self.y_velocity - self.gravity

            self.gravity += 0.01

            if self.rotation < -90:
                self.rotation = -90
                self.low_angle = True

            if not self.low_angle:
                self.rotation_change += 0.002
                self.rotation -= self.rotation_change
                self.spear = kwargs['pygame'].transform.rotate(self.img, self.rotation)

        if self.holding:
            self.x = kwargs['x_player'] + 20

        if self.x - self.center[0] + 100 + self.spear.get_rect()[2] / 2 >= (kwargs['settings'].width / 2):
            self.mid_screen = True

    def draw(self, **kwargs):
        if self.landed:
            kwargs['pygame'].draw.circle(kwargs['screen'], kwargs['color'].brown, (int(self.x - self.center[0] + 100 + self.spear.get_rect()[2] - 5), int(self.y - self.center[1] + self.spear.get_rect()[3] - 5)), 10, 0)

        kwargs['screen'].blit(self.spear, (self.x - self.center[0] + 100, self.y - self.center[1]))
