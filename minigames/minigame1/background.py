class Background:

    def __init__(self, **kwargs):
        self.sky = kwargs['pygame'].image.load(kwargs['settings'].script_dir + "/minigames/minigame1/assets/bg_sky.png").convert()
        self.sky = kwargs['pygame'].transform.scale(self.sky, (1280, 720))
        self.x = 0
        self.y = 0

    def draw(self, **kwargs):
        for x in range(0, 7):
            if x < 2:
                background_img = kwargs['assets'].bg
            else:
                background_img = kwargs['assets'].bg_empty
            
            kwargs['screen'].blit(background_img, (kwargs['settings'].width * x + self.x, self.y))

        for x in range(0, 7):
            kwargs['screen'].blit(self.sky, (kwargs['settings'].width * x + self.x, self.y - kwargs['settings'].height))
