class Popup:

    def __init__(self, **kwargs):
        self.text = kwargs['text']
        self.time_passed = 0
        self.font_name = kwargs['pygame'].font.match_font('Verdana')
        self.font = kwargs['pygame'].font.Font(self.font_name, 30)

    def update(self, **kwargs):
        self.time_passed += kwargs['clock'].get_time()

    def draw(self, **kwargs):
        if self.time_passed < 3000:
            self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], kwargs['settings'].width / 2 - (self.font.size(self.text)[0] + 50) / 2, 70 + self.font.size(self.text)[1] - 25, self.font.size(self.text)[0] + 50, self.font.size(self.text)[1] + 50, kwargs['color'].light_grey, kwargs['color'].black)
            self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], kwargs['settings'].width / 2 - (self.font.size(self.text)[0] + 25) / 2, 70 + self.font.size(self.text)[1] - 12.5, self.font.size(self.text)[0] + 25, self.font.size(self.text)[1] + 25, kwargs['color'].robrecht_blue, kwargs['color'].black)
            self.draw_text(kwargs['screen'], self.text, kwargs['settings'].width / 2 - self.font.size(self.text)[0] / 2, 70 + self.font.size(self.text)[1], kwargs['color'].white)

    def new_popup(self, **kwargs):
        self.text = kwargs['text']
        self.time_passed = 0

    def draw_text(self, surf, text, x, y, color,):
        text_surface = self.font.render(text, True, color)
        surf.blit(text_surface, (x, y))

    def draw_lined_rect(self, pygame, screen, x, y, width, height, rect_color, line_color, line_thickness=2):
        pygame.draw.rect(screen, rect_color, (x, y, width, height))
        pygame.draw.line(screen, line_color, (x, y), (x + width, y), line_thickness)
        pygame.draw.line(screen, line_color, (x, y), (x, y + height), line_thickness)
        pygame.draw.line(screen, line_color, (x, y + height), (x + width, y + height), line_thickness)
        pygame.draw.line(screen, line_color, (x + width, y), (x + width, y + height), line_thickness)
