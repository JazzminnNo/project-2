class HUD:

    def __init__(self, **kwargs):
        self.right_key_img = kwargs['assets'].key_right
        self.right_key_img = kwargs['pygame'].transform.scale(self.right_key_img, (75, 75))
        self.up_key_img = kwargs['assets'].key_up
        self.up_key_img = kwargs['pygame'].transform.scale(self.up_key_img, (72, 72))
        self.down_key_img = kwargs['assets'].key_down
        self.down_key_img = kwargs['pygame'].transform.scale(self.down_key_img, (72, 72))
        self.space_key_img = kwargs['assets'].spacebar
        self.speed = 0
        self.distance = 0
        self.right_active = False
        self.up_active = False
        self.down_active = False
        self.space_active = False
        self.keys_start_pos_x = 1000

    def update(self, **kwargs):
        self.speed = round(kwargs['speed'])
        self.distance = kwargs['distance']

        if self.speed <= 9 / 2:
            self.speed_color = kwargs['color'].green
        elif self.speed >= 9:
            self.speed_color = kwargs['color'].red
        else:
            self.speed_color = kwargs['color'].orange

        if kwargs['inputs'].isset(kwargs['pygame'].K_RIGHT):
            self.right_active = True
        else:
            self.right_active = False

        if kwargs['inputs'].isset(kwargs['pygame'].K_UP):
            self.up_active = True
        else:
            self.up_active = False

        if kwargs['inputs'].isset(kwargs['pygame'].K_DOWN):
            self.down_active = True
        else:
            self.down_active = False

        if kwargs['inputs'].isset(kwargs['pygame'].K_SPACE):
            self.space_active = True
        else:
            self.space_active = False

    def draw(self, **kwargs):
        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 890, 5, 370, 80, kwargs['color'].light_grey, kwargs['color'].black)
        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 905, 15, 185, 60, kwargs['color'].black, kwargs['color'].black)
        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 1100, 15, 140, 60, kwargs['color'].dark_gold, kwargs['color'].black)
        self.draw_text(kwargs['screen'], 'DISTANCE', 30, 1000, 25, kwargs['color'].white, kwargs['pygame'])
        self.draw_text(kwargs['screen'], str(self.distance), 30, 1170, 25, kwargs['color'].black, kwargs['pygame'])

        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 20, 600, 350, 80, kwargs['color'].light_grey, kwargs['color'].black)
        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 35, 610, 120, 60, kwargs['color'].green, kwargs['color'].black)
        self.draw_text(kwargs['screen'], 'SPEED', 30, 95, 620, kwargs['color'].black, kwargs['pygame'])
        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 170, 610, 180, 60, kwargs['color'].dim_gray, kwargs['color'].black)

        if self.speed > 0:
            if self.speed >= 9:
                speed_bar_size = 180
            else:
                speed_bar_size = self.speed * (180 / 9)

            self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 170, 610, speed_bar_size, 60, self.speed_color, kwargs['color'].black)

        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 170, 610, 180, 10, kwargs['color'].black, kwargs['color'].black)
        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 170, 660, 180, 10, kwargs['color'].black, kwargs['color'].black)

        if kwargs['state'] is kwargs['gamestate'].PLAYER:
            self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], self.keys_start_pos_x - 531, 600, 781, 80, kwargs['color'].light_grey, kwargs['color'].black)
            self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], self.keys_start_pos_x - 516, 610, 120, 60, kwargs['color'].robrecht_blue, kwargs['color'].black)
            self.draw_text(kwargs['screen'], 'Move', 30, self.keys_start_pos_x - 456, 620, kwargs['color'].black, kwargs['pygame'])

            if self.right_active:
                self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], self.keys_start_pos_x - 380, 606, 68, 68, kwargs['color'].grey, kwargs['color'].robrecht_blue, 6)

            kwargs['screen'].blit(self.right_key_img, (self.keys_start_pos_x - 381, 605))

            self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], self.keys_start_pos_x - 291, 610, 120, 60, kwargs['color'].robrecht_blue, kwargs['color'].black)
            self.draw_text(kwargs['screen'], 'Spear', 30, self.keys_start_pos_x - 231, 620, kwargs['color'].black, kwargs['pygame'])

            if self.up_active:
                self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], self.keys_start_pos_x - 156, 606, 66, 67, kwargs['color'].grey, kwargs['color'].robrecht_blue, 6)

            kwargs['screen'].blit(self.up_key_img, (self.keys_start_pos_x - 157, 605))

            if self.down_active:
                self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], self.keys_start_pos_x - 76, 606, 65, 66, kwargs['color'].grey, kwargs['color'].robrecht_blue, 6)

            kwargs['screen'].blit(self.down_key_img, (self.keys_start_pos_x - 80, 605))

            if self.space_active:
                self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], self.keys_start_pos_x, 610, 240, 60, kwargs['color'].grey, kwargs['color'].robrecht_blue, 6)

            kwargs['screen'].blit(self.space_key_img, (self.keys_start_pos_x, 610))

    def draw_text(self, surf, text, size, x, y, color, pygame):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        surf.blit(text_surface, text_rect)

    def draw_lined_rect(self, pygame, screen, x, y, width, height, rect_color, line_color, line_thickness=2):
        pygame.draw.rect(screen, rect_color, (x, y, width, height))
        pygame.draw.line(screen, line_color, (x, y), (x + width, y), line_thickness)
        pygame.draw.line(screen, line_color, (x, y), (x, y + height), line_thickness)
        pygame.draw.line(screen, line_color, (x, y + height), (x + width, y + height), line_thickness)
        pygame.draw.line(screen, line_color, (x + width, y), (x + width, y + height), line_thickness)
