from .spear import Spear


class Player:

    def __init__(self, **kwargs):
        self.x = 100
        self.y = 250
        self.speed = 0
        self.velocity = 0
        self.first_movement = True
        self.spear = Spear(spear=kwargs['spear'])

    def update(self, **kwargs):
        if ((kwargs['inputs'].isset(kwargs['pygame'].K_RIGHT) and kwargs['state'] is kwargs['gamestate'].PLAYER) or kwargs['ai'].running) and self.speed < 0.55:
            max_speed = 0.55
            if kwargs['ai'].running:
                if kwargs['difficulty'] is 0:
                    max_speed = 0.35
                elif kwargs['difficulty'] is 1:
                    max_speed = 0.50

            self.speed += 0.025
            if self.speed > max_speed:
                self.speed = max_speed
        elif not (kwargs['inputs'].isset(kwargs['pygame'].K_RIGHT) or (kwargs['ai'].running and kwargs['state'] is kwargs['gamestate'].AI)) and self.speed > 0:
            self.speed -= 0.02
            if self.speed < 0:
                self.speed = 0

        self.velocity = self.speed * kwargs['settings'].tick

        if self.x >= (kwargs['settings'].width / 2) - 100 and self.first_movement:
            self.first_movement = False

        if self.spear.mid_screen and self.x > -200 and not self.spear.holding:
            self.x -= self.spear.x_velocity
        elif self.first_movement and self.spear.holding:
            self.x += self.velocity

        self.spear.update(**kwargs, x_player=self.x, speed=self.velocity)

    def draw(self, **kwargs):
        kwargs['screen'].blit(kwargs['assets'].blob3, (self.x, self.y))
        self.spear.draw(**kwargs)
