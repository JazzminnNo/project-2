from .player import Player
from .hud import HUD
from .background import Background
from .ai import AI
from .popup import Popup
from enum import Enum


class GameState(Enum):
    AI = 0
    PLAYER = 1


class Minigame1:

    def __init__(self, pygame, settings, highscore, sound):
        self.spear_image = pygame.image.load(settings.script_dir + "/minigames/minigame1/assets/spear.png").convert_alpha()
        self.spear_image = pygame.transform.scale(self.spear_image, (600, 11))
        self.hs = highscore
        self.sound = sound

    def start(self, **kwargs):
        if 'selected_button' in kwargs:
            self.difficulty = kwargs['selected_button']

        self.switch_state_time = 0
        self.started = False
        self.state = GameState.AI
        self.ai = AI(difficulty=self.difficulty)
        self.popup = Popup(**kwargs, text="Computer's turn")
        self.reset_game(**kwargs)
        self.highscore = self.hs.load_highscore_speer(self.difficulty)

    def reset_game(self, **kwargs):
        self.player = Player(spear=self.spear_image)
        self.hud = HUD(**kwargs)
        self.background = Background(**kwargs)
        self.camera_moving = True

    def update(self, **kwargs):
        if kwargs['inputs'].isset(kwargs['pygame'].K_SPACE):
            self.started = True

        if self.started:
            if self.state is GameState.AI and self.player.spear.landed:
                self.switch_state_time += kwargs['clock'].get_time()

                if self.switch_state_time > 2000:
                    self.ai.score = self.get_distance_spear()
                    self.state = GameState.PLAYER
                    self.reset_game(**kwargs)
                    self.popup.new_popup(text="Your turn")

            if self.state is GameState.PLAYER and self.player.spear.landed:
                result_text = ""
                if self.get_distance_spear() <= self.ai.score:
                    result_text = "You got a score of: " + str(self.get_distance_spear()) + "m\n\nYou didn't beat the computer's score of: " + str(self.ai.score) + "m"
                    self.sound.crowd_boo_sound.play()
                elif self.get_distance_spear() <= self.highscore:
                    result_text = "You got a score of: " + str(self.get_distance_spear()) + "m\n\nYou didn't beat the highscore of: " + str(self.highscore) + "m"
                    self.sound.crowd_boo_sound.play()
                else:
                    result_text = "You got a new highscore of: " + str(self.get_distance_spear()) + "m!"
                    self.sound.partyhorn.play()
                    self.sound.crowd_cheer_sound.play()
                    self.hs.save_highscore(self.get_distance_spear(), 1, self.difficulty)

                kwargs['menu'].score_screen_show(**kwargs, text=result_text)

            if self.state is GameState.PLAYER and self.player.x + kwargs['assets'].blob2.get_rect().size[0] > 2560 + self.background.x and self.player.spear.holding:
                kwargs['menu'].score_screen_show(**kwargs, text="You have to throw the spear while on the running track! No points!")
                self.sound.crowd_boo_sound.play()

            if self.state is GameState.AI:
                self.ai.update(**kwargs, player=self.player, background_x=self.background.x)

            self.player.update(**kwargs, bg_y=self.background.y, camera_moving=self.camera_moving, ai=self.ai, state=self.state, gamestate=GameState, difficulty=self.difficulty)

            if self.player.spear.mid_screen and not self.player.spear.holding and not self.player.spear.landed:
                self.background.x -= self.player.spear.x_velocity
            elif not self.player.first_movement and self.player.spear.holding:
                self.background.x -= self.player.velocity

            if not self.player.spear.holding and not self.player.spear.landed and self.camera_moving:
                if (self.background.y + self.player.spear.y_velocity - self.player.spear.gravity) > 0:
                    self.background.y += self.player.spear.y_velocity - self.player.spear.gravity
                    self.player.y += self.player.spear.y_velocity - self.player.spear.gravity
                else:
                    self.background.y = 0
                    self.player.y = 250
                    self.camera_moving = False

            self.hud.update(**kwargs, speed=self.player.velocity, distance=self.get_distance_spear())
            self.popup.update(**kwargs)

    def draw(self, **kwargs):
        kwargs['screen'].fill(kwargs['color'].blue_bg)
        self.background.draw(**kwargs)
        self.player.draw(screen=kwargs['screen'], assets=kwargs['assets'], pygame=kwargs['pygame'], color=kwargs['color'])
        self.hud.draw(**kwargs, state=self.state, gamestate=GameState)
        self.popup.draw(**kwargs)

        if not self.started:
            self.show_start_screen(kwargs['screen'], kwargs['settings'], kwargs['pygame'], kwargs['color'])

    def show_start_screen(self, screen, settings, pygame, color):
        image = pygame.Surface([settings.width, settings.height]).convert()
        image.set_alpha(200)
        image.fill((0, 0, 0))
        rect = image.get_rect()
        rect.center = (settings.width / 2, (settings.height / 2))
        screen.blit(image, rect)
        self.draw_text(screen, 'Javelin!', 64, settings.width / 2, settings.height / 4, color.white, pygame)
        self.draw_text(screen, 'Press Space to start the game', 22, settings.width / 2, settings.height / 2, color.white, pygame)
        self.draw_text(screen, 'The highscore to beat is: ' + str(self.highscore) + "m", 22, settings.width / 2, settings.height / 2 + 60, color.white, pygame)

    def draw_text(self, surf, text, size, x, y, color, pygame):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        surf.blit(text_surface, text_rect)

    def get_distance_spear(self):
        distance = round(((self.player.spear.x - self.player.spear.center[0] + 100 + self.player.spear.spear.get_rect()[2]) - 2560 - self.background.x) / 50)

        if distance < 0:
            distance = 0

        return distance
