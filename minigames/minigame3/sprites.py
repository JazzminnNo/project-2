import os
import pygame
from .mini3settings import *
vec = pygame.math.Vector2
game_folder = os.path.dirname(__file__)
img_folder = os.path.join(game_folder, "Images")

green = [0, 100, 50]
realgreen = [0, 255, 50]
black = [0, 0, 0]

class Player(pygame.sprite.Sprite):

    def __init__(self, game, settings, pygame, assets, sound):
        pygame.sprite.Sprite.__init__(self)
        self.game = game
        self.blobs = [""]
        self.image = assets.blob2
        #os.path.join(img_folder, "Blob2.png")
        self.image.set_colorkey(black)
        self.image = pygame.transform.scale(self.image, (230, 230))
        #de rect that surrounds the image/the sprite
        self.rect = self.image.get_rect()
        self.rect.center = (settings.width / 2, settings.height / 2)
        self.vec = pygame.math.Vector2
        self.pos = (settings.width / 2, settings.height - 220) #vec( HEIGHT / 2)
        self.vel = vec(0, 0)
        self.acc = vec(0, 0)
        self.player_acc = 0.5
        self.sound = sound

    def update(self):
        self.acc = vec(0, PLAYER_GRAV)
        self.vel += self.acc
        self.pos += (self.vel + 0.5 * self.acc)

        self.rect.midbottom = self.pos

    def jump(self):
        # jump only if standing on a platform
        self.rect.x += 1
        self.hits = pygame.sprite.spritecollide(self, self.game.platforms, False)
        self.rect.x -= 1
        if self.hits:
            self.vel.y = -20

class Background(pygame.sprite.Sprite):

    def __init__(self, game, settings):
        pygame.sprite.Sprite.__init__(self)
        self.groups = game.all_sprites, game.background
        self.image = pygame.image.load(os.path.join(img_folder, "blob_veld_metlucht_1280_720_donkercrowd.png")).convert()
        w, h = self.image.get_size()
        self.image = pygame.transform.smoothscale(self.image, (w, h))
        # de rect that surrounds the image/the sprite
        self.rect = self.image.get_rect()
        self.rect.bottom = settings.height + 125

    def draw_text(surf, text, size, x, y, color):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)  # lettertype en lettergrootte
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        surf.blit(text_surface, text_rect)

    def draw_rectangle(surf, width, height, x, y, color, alpha):
        image = pygame.Surface([width, height]).convert()
        image.set_alpha(alpha)  # , pygame.SRCALPHA, 32)
        image.fill(color)
        rect = image.get_rect()
        rect.center = (x, y)
        surf.blit(image, rect)

    def draw_finish(surf, settings):
        image = pygame.Surface([settings.width, 150]).convert()
        image.set_alpha(200)  # , pygame.SRCALPHA, 32)
        image.fill((0, 0, 0))
        # image = image.convert_alpha()
        rect = image.get_rect()
        rect.center = (settings.width / 2, (settings.height / 2))
        surf.blit(image, rect)


class Platform(pygame.sprite.Sprite):

    def __init__(self, x, y, w, h):
        pygame.sprite.Sprite.__init__(self)
        self.image = pygame.Surface((w, h))
        self.image.fill(realgreen)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

class FloatingButton(pygame.sprite.Sprite):

    def __init__(self, letter, y, screen, pygame, color):
        pygame.sprite.Sprite.__init__(self)
        self.rect_with_text(pygame, screen, y, 60, 60, (200, 0, 50), color, letter, 46, (255,255,255), 255, 2)

    def rect_with_text(self, pygame, screen, pos_y, width, height, rect_color, line_color,  text, text_size, text_color, alpha, line_thickness):
        self.draw_lined_rect(pygame, screen, pos_y, width, height, rect_color, line_color, alpha, line_thickness)
        self.draw_text(text, text_size, text_color)

    def draw_lined_rect(self, pygame, screen, y, width, height, rect_color, line_color, alpha=255, line_thickness=5):

        self.y = y
        self.width = width
        self.height = height
        self.rect_color = rect_color
        self.line_color = line_color
        self.line_thickness = line_thickness

        self.image = pygame.Surface((width, height))  # the size of your rect
        self.image.set_alpha(alpha)  # alpha level
        self.image.fill(rect_color)  # this fills the entire surface
        self.rect = self.image.get_rect()
        self.rect.center = (200, y)
         # (0,0) are the top-left coordinates
#Under line
        self.line =  pygame.Surface((width, self.line_thickness))
        self.line.fill(self.line_color)
        self.line_rect = self.line.get_rect()
        self.line_rect.midbottom = (width/2, height)
        self.image.blit(self.line, self.line_rect)
#Top line
        self.line2 = pygame.Surface((width, self.line_thickness))
        self.line2.fill(self.line_color)
        self.line2_rect = self.line2.get_rect()
        self.line2_rect.midbottom = (width/2, self.line_thickness)
        self.image.blit(self.line2, self.line2_rect)

        self.line3 = pygame.Surface((self.line_thickness, height))
        self.line3.fill(self.line_color)
        self.line3_rect = self.line3.get_rect()
        self.line3_rect.midbottom = (width -(self.line_thickness/2), height)
        self.image.blit(self.line3, self.line3_rect)

        self.line4 = pygame.Surface((self.line_thickness, height))
        self.line4.fill(self.line_color)
        self.line4_rect = self.line4.get_rect()
        self.line4_rect.midbottom = (self.line_thickness/2, height)
        self.image.blit(self.line4, self.line4_rect)

        screen.blit(self.image, self.rect)

    def draw_text(self, text, size, color):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)  # lettertype en lettergrootte
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midbottom = (29, 55)
        self.image.blit(text_surface, text_rect)








