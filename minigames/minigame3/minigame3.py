from .sprites import *
import random

class Minigame3():

    def __init__(self, pygame, settings, color, screen, assets, inputs, highscore, sound):
        self.inputs = inputs
        self.color = color
        self.h = highscore
        self.sound = sound
        self.pygame = pygame
        self.settings = settings
        self.assets = assets
        self.p1 = Platform(200, settings.height - 220, 400, 40)
        self.screen = screen
        self.spacebarImg = self.assets.spacebar
        self.key_w = self.assets.key_w
        self.key_a = self.assets.key_a
        self.key_s = self.assets.key_s
        self.key_d = self.assets.key_d

    def start(self, **kwargs):
        self.running = True
        try:
            self.difficulty = kwargs['selected_button']
        except:
            pass
        self.text = ''
        self.special_color = (0,0,0)
        if self.difficulty == 0:
            self.fps = 25
            self.text = 'Easy'
            self.special_color = self.color.green
        if self.difficulty == 1:
            self.fps = 40
            self.text = 'Medium'
            self.special_color = self.color.orange
        if self.difficulty == 2:
            self.fps = 60
            self.text = 'Hard'
            self.special_color = self.color.red
        self.highscore = self.h.load_highscore_trampoline(self.difficulty)
        self.all_sprites = pygame.sprite.Group()
        self.platforms = pygame.sprite.Group()
        self.background = pygame.sprite.Group()
        self.floating_button = pygame.sprite.Group()
        self.pc = pygame.sprite.Group()
        self.player = Player(self, self.settings, pygame, self.assets, self.sound)
        b1 = Background(self, self.settings)
        self.yposition = 100
        self.possible_letters = ['A', 'W', 'S', 'D']
        self.letter = random.choice(self.possible_letters)
        self.float_color = self.color.black
        self.f1 = FloatingButton(self.letter, self.yposition, self.screen, self.pygame, self.float_color)
        self.background.add(b1)
        self.platforms.add(self.p1)
        self.pc.add(self.player)
        self.all_sprites.add(self.p1)  # Layers
        self.all_sprites.add(b1)
        self.jump_count_down = 9
        self.jump_count_up = 0
        self.timer = 1
        self.dt = 0
        self.score = 0
        self.index = 0
        self.current_letter = []
        self.current_button = []
        self.buttons = []
        self.button_killed = False
        self.spawn = True
        self.jump_count_done = True
        self.jump_count_done_up = True
        self.isJumping = False
        self.autoJump = False
        self.end = False
        self.jump_end = False
        self.started = False
        self.remove = False
        self.show_finish = False
        self.lock = False
        self.music_play = True
        self.cheer_play = True
        self.crowd_response_play = True
        self.draw_text = False
        self.current_sentence = []
        self.inputKeysDefaultColor = self.color.grey
        self.inputLetterWColor = self.inputKeysDefaultColor
        self.inputLetterAColor = self.inputKeysDefaultColor
        self.inputLetterSColor = self.inputKeysDefaultColor
        self.inputLetterDColor = self.inputKeysDefaultColor
        self.inputSpaceKeyBorderColor = self.inputKeysDefaultColor

    def update(self, **kwargs):
        self.settings = kwargs['settings']
        self.inputs = kwargs['inputs']
        self.pygame = kwargs['pygame']
        self.clock = pygame.time.Clock()
        self.clock.tick(self.fps)

        if self.inputs.down(self.pygame.K_w):
            self.inputLetterWColor = self.color.red
        else:
            self.inputLetterWColor = self.inputKeysDefaultColor

        if self.inputs.down(self.pygame.K_a):
            self.inputLetterAColor = self.color.blue
        else:
            self.inputLetterAColor = self.inputKeysDefaultColor

        if self.inputs.down(self.pygame.K_s):
            self.inputLetterSColor = self.color.yellow
        else:
            self.inputLetterSColor = self.inputKeysDefaultColor

        if self.inputs.down(self.pygame.K_d):
            self.inputLetterDColor = self.color.green
        else:
            self.inputLetterDColor = self.inputKeysDefaultColor

        if self.inputs.down(self.pygame.K_SPACE):
            self.inputSpaceKeyBorderColor = self.color.purple
        else:
            self.inputSpaceKeyBorderColor = self.inputKeysDefaultColor

        if self.inputs.down(pygame.K_SPACE):
            self.started = True
            if not self.jump_end:
                self.player.jump()
                if self.player.pos == (self.settings.width / 2, self.settings.height - 220):
                    if self.jump_count_done and self.jump_count_done_up:
                        self.jump_count_up += 1
                        self.isJumping = True
                        self.jump_count_done_up = False
                        self.sound.jump_sound.play()
                        if self.jump_count_up > 4:
                            self.jump_count_down -= 1
                            self.jump_count_done = False
        if self.inputs.down(pygame.K_s):
            if not self.lock:
                if self.index < len(self.current_letter):
                    if self.jump_count_up >= 4:
                        self.draw_text = False
                        if not self.draw_text:
                            if self.current_letter[self.index] == 'S':
                                if self.current_button != []:
                                    self.floating_button.remove(self.current_button[0])
                                self.index += 1
                                self.draw_text = True
                                self.button_killed = True
                                self.sound.key_hit_sound.play()
                                self.score += 100
        if self.inputs.down(pygame.K_w):
            if not self.lock:
                if self.index < len(self.current_letter):
                    if self.jump_count_up >= 4:
                        self.draw_text = False
                        if not self.draw_text:
                            if self.current_letter[self.index] == 'W':
                                if self.current_button != []:
                                    self.floating_button.remove(self.current_button[0])
                                self.index += 1
                                self.draw_text = True
                                self.button_killed = True
                                self.sound.key_hit_sound.play()
                                self.score += 100
        if self.inputs.down(pygame.K_d):
            if not self.lock:
                if self.index < len(self.current_letter):
                    if self.jump_count_up >= 4:
                        self.draw_text = False
                        if not self.draw_text:
                            if self.current_letter[self.index] == 'D':
                                if self.current_button != []:
                                    self.floating_button.remove(self.current_button[0])
                                self.index += 1
                                self.draw_text = True
                                self.button_killed = True
                                self.sound.key_hit_sound.play()
                                self.score += 100
        if self.inputs.down(pygame.K_a):
            if not self.lock:
                if self.index < len(self.current_letter):
                    if self.jump_count_up >= 4:
                        self.draw_text = False
                        if not self.draw_text:
                            if self.current_letter[self.index] == 'A':
                                if self.current_button != []:
                                    self.floating_button.remove(self.current_button[0])
                                self.index += 1
                                self.draw_text = True
                                self.button_killed = True
                                self.sound.key_hit_sound.play()
                                self.score += 100
        self.all_sprites.update()
        self.pc.update()

        hits = pygame.sprite.spritecollide(self.player, self.platforms, False)
        if hits:
            self.player.pos.y = hits[0].rect.top
            self.player.vel.y = 0

        if self.player.pos == (self.settings.width / 2, self.settings.height - 220):
            self.lock = True
        elif self.player.pos != (self.settings.width / 2, self.settings.height - 220):
            self.lock = False

        if not (self.player.pos == (self.settings.width / 2, self.settings.height - 220)):
            if not self.jump_count_done:
                self.jump_count_done = not self.jump_count_done

        if not self.player.pos == (self.settings.width / 2, self.settings.height - 220):
            if not self.jump_count_done_up:
                self.jump_count_done_up = not self.jump_count_done_up

        if self.autoJump:
            if self.jump_count_up >= 1 and self.player.pos == (self.settings.width / 2, self.settings.height - 220):
                self.player.jump()
                self.jump_count_up += 1
                self.jump_count_down -= 1
                if self.player.vel.y != 0 and not self.removed:
                    self.button_killed = True
                    for float in self.floating_button:
                        self.floating_button.remove(float)
                    self.yposition = 100
                self.autoJump = False
                if not(self.autoJump and self.jump_count_down == -1 and self.player.pos == (self.settings.width / 2, self.settings.height - 220)):
                    self.show_finish = True

        if self.jump_count_down == 0 and self.player.pos == (self.settings.width / 2, self.settings.height - 220):
            self.removed = False
            self.autoJump = True

        if self.jump_count_down == 0:
            self.jump_end = True

        if self.end and self.jump_count_down == -1 and self.player.pos == (self.settings.width / 2, self.settings.height - 220):
            self.running = False

        if self.player.vel.y > 0:
            for back in self.background:
                back.rect.y -= 23
        elif self.player.vel.y < 0:
            for back in self.background:
                back.rect.y += 23

        if self.jump_count_up >= 4:
            if self.spawn:
                del self.current_button[:]
                del self.current_letter[:]
                self.index = 0
                for i in range(8):
                    self.letter = random.choice(self.possible_letters)
                    self.current_letter.append(self.letter)
                    self.f1 = FloatingButton(self.letter, self.yposition, self.screen, self.pygame, self.float_color)
                    self.floating_button.add(self.f1)
                    for float in self.floating_button:
                        self.current_button.append(float)
                    self.yposition = self.yposition + 65
                    self.button_killed = False
                    self.spawn = False
                    self.removed = False
            if self.button_killed:
                del self.current_button[:]
                for float in self.floating_button:
                    self.current_button.append(float)
                self.button_killed = False

            if self.player.vel.y == 0 and not self.removed:
                for float in self.floating_button:
                    self.floating_button.remove(float)
                self.draw_text = False
                self.removed = True
                self.spawn = True
                self.yposition = 100

        if not self.running:
            if self.score > self.highscore:
                kwargs['menu'].score_screen_show(**kwargs, text='Score:   ' + str(self.score) + "\n\nA new highscore is achieved! It's now:  " + str(self.score))
                self.h.save_highscore(self.score, 3, self.difficulty)
                if self.crowd_response_play is True:
                    self.sound.partyhorn.play()
                    self.sound.crowd_cheer_sound.play()
                    self.pygame.mixer.fadeout(18000)
                    self.crowd_response_play = False
            else:
                kwargs['menu'].score_screen_show(**kwargs, text='Score:   ' + str(self.score) + "\n\nSuch a shame! The highscore to beat is: " + str(self.highscore))
                if self.crowd_response_play is True:
                    self.sound.cue_scratch.play()
                    self.sound.crowd_boo_sound.play()
                    self.pygame.mixer.fadeout(18000)
                    self.crowd_response_play = False

        if self.jump_count_down <= 0 and self.player.pos == (self.settings.width / 2, self.settings.height - 220):
            self.spawn = False

        self.floating_button.update()

    def draw(self, **kwargs):
        self.screen = kwargs['screen']
        self.settings = kwargs['settings']
        self.pygame = kwargs['pygame']
        self.screen.fill(green)
        self.all_sprites.draw(self.screen)
        if self.started and not self.spawn and self.running:
            self.show_sentences(self.screen, self.settings)
        self.pc.draw(self.screen)
        self.floating_button.draw(self.screen)
        if self.jump_count_up < 4 and self.jump_count_up != 0:
            Background.draw_text(self.screen, str(self.jump_count_up), 100, self.settings.width / 2, 50, WHITE)
        if self.jump_count_up == 4:
            Background.draw_text(self.screen, 'START', 100, self.settings.width / 2, 50, WHITE)

        if self.jump_count_up >= 4 and self.jump_count_down >= 0:

            if self.draw_text:
                Background.draw_text(self.screen, 'Hit!', 50, 300, 150, WHITE)
            # TIME CONTAINER
            self.draw_lined_rect(self.pygame, self.screen, 1065, 5, 200, 140, self.color.light_grey, self.color.black)
            # TIME
            self.draw_lined_rect(self.pygame, self.screen, 1081, 15, 170, 60, self.color.black, self.color.black)
            # TIMER
            self.draw_lined_rect(self.pygame, self.screen, 1081, 75, 170, 60, self.color.dark_gold, self.color.black)

            # SPEED CONTAINER
            self.draw_lined_rect(self.pygame, self.screen, 20, 620, 350, 80, self.color.light_grey, self.color.black)
            # SPEED
            self.draw_lined_rect(self.pygame, self.screen, 35, 630, 180, 60, self.color.green, self.color.black)
            # SPEED BAR CONTAINER
            self.draw_lined_rect(self.pygame, self.screen, 230, 630, 120, 60, self.color.dark_gold, self.color.black)
            # SPEED BAR

            # KEYS CONTAINER
            self.draw_lined_rect(self.pygame, self.screen, 450, 620, 800, 80, self.color.light_grey, self.color.black)
            # MOVE
            self.draw_lined_rect(self.pygame, self.screen, 465, 630, 120, 60, self.color.robrecht_blue,
                                 self.color.black)
            if self.player.pos != (self.settings.width / 2, self.settings.height - 220) and self.jump_count_up >= 4:
                # KEY W
                self.draw_lined_rect(self.pygame, self.screen, 600, 636, 50, 50, self.color.grey, self.inputLetterWColor, 6)
                self.screen.blit(self.key_w, (600, 636))
                # KEY A
                self.draw_lined_rect(self.pygame, self.screen, 660, 636, 50, 50, self.color.grey, self.inputLetterAColor, 6)
                self.screen.blit(self.key_a, (660, 636))
                # KEY S
                self.draw_lined_rect(self.pygame, self.screen, 720, 636, 50, 50, self.color.grey, self.inputLetterSColor, 6)
                self.screen.blit(self.key_s, (720, 636))
                # KEY D
                self.draw_lined_rect(self.pygame, self.screen, 780, 636, 50, 50, self.color.grey, self.inputLetterDColor, 6)
                self.screen.blit(self.key_d, (780, 636))
            # KEY W
            self.screen.blit(self.key_w, (600, 636))
            # KEY A
            self.screen.blit(self.key_a, (660, 636))
            # KEY S
            self.screen.blit(self.key_s, (720, 636))
            # KEY D
            self.screen.blit(self.key_d, (780, 636))

            # JUMP
            self.draw_lined_rect(self.pygame, self.screen, 850, 630, 120, 60, self.color.robrecht_blue, self.color.black)
            # JUMP BUTTON
            self.draw_lined_rect(self.pygame, self.screen, 1000, 630, 240, 60, self.color.black, self.inputSpaceKeyBorderColor, 6)
            self.screen.blit(self.spacebarImg, (1000, 630))

            # MOVE TEXT
            Background.draw_text(self.screen, 'Keys:', 30, 525, 640, self.color.black)

            # JUMP TEXT
            Background.draw_text(self.screen, 'Jump:', 30, 910, 640, self.color.black)
            # SPACE TEXT
            Background.draw_text(self.screen, 'Jumps left: ', 25, 120, 645, BLACK)
            Background.draw_text(self.screen, str(self.jump_count_down), 25, 290, 645, BLACK)
            Background.draw_text(self.screen, 'SCORE  ', 30, 1180 , 28, WHITE)
            Background.draw_text(self.screen, str(self.score), 25, 1170, 90, BLACK)
        if not self.started:
            self.show_start_screen(self.screen, self.settings)
        if self.jump_count_down <= 3 and self.jump_count_down > 0:
            Background.draw_text(self.screen, str(self.jump_count_down), 100, self.settings.width / 2, 50, WHITE)
        if self.jump_count_down == 0:
            Background.draw_text(self.screen, 'Last jump!', 100, self.settings.width / 2, 50, WHITE)
        if self.show_finish:
            Background.draw_finish(self.screen, self.settings)
            Background.draw_text(self.screen, 'FINISH!', 100, self.settings.width / 2, self.settings.height / 2 - 63, WHITE)
            self.end = True
        if not self.running:
            self.show_finish = False

    def show_start_screen(self, screen, settings):
        image = pygame.Surface([settings.width, settings.height]).convert()
        image.set_alpha(200)  # , pygame.SRCALPHA, 32)
        image.fill((0, 0, 0))
        rect = image.get_rect()
        rect.center = (settings.width / 2, (settings.height / 2))
        screen.blit(image, rect)
        Background.draw_text(screen, 'Trampoline!', 64, settings.width / 2, settings.height / 4, WHITE)  # game splas/start screen
        Background.draw_text(screen, 'Press Space to start the game', 22, settings.width / 2, settings.height / 2, WHITE)  # game splas/start screen
        Background.draw_text(screen, 'Highscore is: ' + str(self.highscore), 22, settings.width / 2, settings.height / 2 + 60,
                             WHITE)

    def show_sentences(self, screen, settings):
        self.index_sentence = 0
        self.sentence = ['Go Go!', 'You are the best!', 'This is DogeStyle!', 'WOOHOOOOOOO!', 'Yeeey!', 'Just do it!', 'Beautiful!']
        self.x = random.randint(200, settings.width - 200)
        self.y = random.randint(150,  250)
        self.current_sentence = random.choice(self.sentence)

        Background.draw_text(screen, self.current_sentence, 30, self.x, self.y, WHITE)

    def draw_lined_rect(self, pygame, screen, x, y, width, height, rect_color, line_color, alpha=255, line_thickness=2):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.rect_color = rect_color
        self.line_color = line_color
        self.line_thickness = line_thickness

        s = pygame.Surface((width, height))  # the size of your rect
        s.set_alpha(alpha)  # alpha level
        s.fill(rect_color)  # this fills the entire surface
        screen.blit(s, (x, y))  # (0,0) are the top-left coordinates

        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x + self.width, self.y), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y + self.height), (self.x + self.width, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x + self.width, self.y), (self.x + self.width, self.y + self.height), self.line_thickness)

    def rect_with_text(self, pygame, screen, surf, pos_x, pos_y, width, height, rect_color, line_color,  text, text_size, text_color, alpha=255, line_thickness=2):
        self.draw_lined_rect(pygame, screen, pos_x, pos_y, width, height, rect_color, line_color, alpha, line_thickness)
        Background.draw_text(surf, text, text_size, pos_x + (width/2), pos_y + (height/4), text_color)
