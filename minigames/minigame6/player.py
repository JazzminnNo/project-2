class Player:

    def __init__(self, **kwargs):
        self.x = 100
        self.y = 300
        self.gravity = 6
        self.velocity = 6
        self.resistance = 8
        self.countSpaceBar = 0
        self.countList = []
        self.movement = True
        self.jump = False
        self.fail = False
        self.started = False


    def update(self, **kwargs):

        if self.x < 1100 and self.started:
            if kwargs['inputs'].isset(kwargs['pygame'].K_RIGHT) and self.movement:
                self.x += self.resistance

        if self.x < 640 and self.started:
            if kwargs['inputs'].isset(kwargs['pygame'].K_SPACE) and not(self.jump) and not self.fail:
                self.countSpaceBar += 1
                self.countList.append(self.countSpaceBar)
                print(len(self.countList))
                if len(self.countList) < 31:
                    self.y -= self.velocity - self.gravity
                    self.velocity += 1

        # Falling down
        if self.x <= 640 and self.y <= 280:
            self.y += self.gravity
        if self.x > 640 and self.y <= 220:
            self.y += self.gravity
        if self.x > 980 and self.y <= 280:
            self.y += self.gravity

        #Collision
        if self.x >= 640 and self.x <= 785 and self.y <= 300 and self.y >= 140 and self.started:
            self.movement = False
            self.fail = True
            self.started = False
        else:
            None

        #Pass or no Pass
        if self.x > 785 and self. x <= 980 and self.y > 210 and self.started:
            self.movement = False
            self.fail = False
            self.started = False

        elif self.x > 980 and self.y > 270 and self.started:
            self.movement = False
            self.fail = True
            self.started = False
        else:
            None



    def draw(self, assets, **kwargs):

            kwargs['screen'].blit(assets.blob2, (self.x, self.y))






