from .player import Player
from .bar import Bar
from .hud import HUD
import json
import os

class Minigame6:

    def __init__(self, pygame, settings, highscore, sound):
        self.pygame = pygame
        self.angleImage = pygame.image.load(settings.script_dir + "/minigames/minigame6/assets/Angle.png")\
            .convert_alpha()
        self.barfaal= pygame.image.load(
            settings.script_dir + "/minigames/minigame6/assets/hoogspringenfaal.png").convert_alpha()
        self.passes = pygame.image.load(settings.script_dir + "/minigames/minigame6/assets/pass.png").convert_alpha()
        self.failure = pygame.image.load(settings.script_dir + "/minigames/minigame6/assets/Fail.png").convert_alpha()

        self.scores = os.path.dirname(os.path.abspath(__file__))
        self.score = os.path.join(self.scores, "scores.json")
        self.sound = sound
        self.hs = highscore

    def start(self, **kwargs):

        if 'selected_button' in kwargs:
            self.difficulty = kwargs['selected_button']

        if self.difficulty == 0:
            self.player = Player(pygame=kwargs['pygame'])
            pass
        if self.difficulty == 1:
            self.player = Player(pygame=kwargs['pygame'])
            self.player.resistance = 16
        if self.difficulty == 2:
            self.player = Player(pygame=kwargs['pygame'])
            self.player.resistance = 25

        self.started = False

        self.bar = Bar(pygame= kwargs['pygame'], settings= kwargs['settings'])

        self.background = 0
        self.highscore = self.hs.load_highscore_hoogspringen(self.difficulty)

        self.hud = HUD(**kwargs)
        self.start_game = True
        self.cheer = True
        self.ratio = 0
        self.passed = 0
        self.failures = 0
        self.number = self.difficulty

    def update(self, **kwargs):

        if kwargs['inputs'].isset(kwargs['pygame'].K_SPACE) and not self.started:
            self.started = True
        if kwargs['inputs'].isset(kwargs['pygame'].K_RIGHT) and self.started:
            self.player.countSpaceBar = 0
            self.player.started = True

        if kwargs['inputs'].isset(kwargs['pygame'].K_SPACE) and self.player.y == 290:
            self.sound.jump_sound.play()

        self.player.update(pygame=kwargs['pygame'], inputs=kwargs['inputs'], settings=kwargs['settings'])
        self.hud.update(**kwargs)

        #Highscore Funtcion

        if not self.player.movement and not self.player.fail and not self.player.started and self.start_game is True:

            self.calculate_highscore_pass()
            self.hs.save_highscore(int(self.ratio), 6, self.difficulty)
            if self.cheer is True:
                self.sound.crowd_cheer_sound.play()
                self.pygame.mixer.fadeout(10000)
                self.cheer = False

            self.start_game = False
            kwargs['menu'].score_screen_show(**kwargs, text="PASS! \n\n You have passed : " +
                                                            str(self.passed) + " times and failed : " +
                                                            str(self.failures) + " times. \n\n Your chance to pass is : "
                                                            + str(int(self.ratio)) + "%" )

        elif not self.player.movement and self.player.fail and not self.player.started and self.start_game is True:

            self.calculate_highscore_fail()
            self.hs.save_highscore(int(self.ratio), 6, self.difficulty)
            if self.cheer is True:
                self.sound.crowd_boo_sound.play()
                self.cheer = False
                self.pygame.mixer.fadeout(10000)

            self.start_game = False
            kwargs['menu'].score_screen_show(**kwargs, text="FAIL! \n \n You have passed : " +
                                                            str(self.passed) + " times and failed : " +
                                                            str(self.failures) + " times. \n Your chance to pass is : "
                                                            + str(int(self.ratio)) + "%" )

    #Drawfunction
    def draw(self, **kwargs):

        kwargs['screen'].blit(kwargs['assets'].bg, (self.background, 0))
        self.hud.draw(**kwargs)
        self.player.draw(screen=kwargs['screen'], assets=kwargs['assets'], pygame=kwargs['pygame'])

        if not self.player.fail:
            self.bar.draw(**kwargs)

        else:
            kwargs['screen'].blit(self.barfaal, (800, 300))
            kwargs['screen'].blit(self.failure, (500, 300))

        if not self.player.fail and not self.player.movement:
            kwargs['screen'].blit(self.passes, (500, 300))

        if not self.started:
            self.show_start_screen(kwargs['screen'], kwargs['settings'], kwargs['pygame'], kwargs['color'])

    def show_start_screen(self, screen, settings, pygame, color):
        image = pygame.Surface([settings.width, settings.height]).convert()
        image.set_alpha(200)
        image.fill((0, 0, 0))
        rect = image.get_rect()
        rect.center = (settings.width / 2, (settings.height / 2))
        screen.blit(image, rect)
        self.draw_text(screen, 'High Jumping', 64, settings.width / 2, settings.height / 4, color.white, pygame)
        self.draw_text(screen, 'Press Space to start the game', 22, settings.width / 2, settings.height / 2, color.white
                       , pygame)

    def draw_text(self, surf, text, size, x, y, color, pygame):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        surf.blit(text_surface, text_rect)

    def calculate_highscore_pass(self):

        with open(self.score, "r") as f:
            countScores = json.load(f)

        temp = countScores[str(self.number)]["totalPasses"]
        countScores[str(self.number)]["totalPasses"] = temp + 1
        self.failures = countScores[str(self.number)]['totalFailures']
        self.passed = countScores[str(self.number)]["totalPasses"]

        total = self.passed + self.failures

        if self.passed > 0:
            self.ratio = self.passed / total * 100
        print(self.ratio)
        with open(self.score, "w") as f:
            json.dump(countScores, f)
        return self.ratio, self.passed, self.failures

    def calculate_highscore_fail(self):

        with open(self.score, "r") as f:
            countScores = json.load(f)

        self.passed = countScores[str(self.number)]["totalPasses"]
        temp = countScores[str(self.number)]["totalFailures"]
        countScores[str(self.number)]["totalFailures"] = temp + 1
        self.failures = countScores[str(self.number)]['totalFailures']

        total = self.passed + self.failures

        if self.passed > 0:
            self.ratio = self.passed / total * 100
        print(self.ratio)
        with open(self.score, "w") as f:
            json.dump(countScores, f)
        return self.ratio, self.passed, self.failures