class HUD:

    def __init__(self, **kwargs):
        self.rightKey = kwargs['assets'].key_right
        self.rKeyImage = kwargs['pygame'].transform.scale(self.rightKey, (75, 75))
        self.spaceBar = kwargs['assets'].spacebar

    def update(self, **kwargs):
        if kwargs['inputs'].isset(kwargs['pygame'].K_RIGHT):
            self.right_active = True
        else:
            self.right_active = False

        if kwargs['inputs'].isset(kwargs['pygame'].K_SPACE):
            self.space_active = True
        else:
            self.space_active = False

    def draw(self, **kwargs):

        self.draw_text(kwargs['screen'], 'MOVE : ', 30, 320, 620, kwargs['color'].black, kwargs['pygame'])
        if self.right_active:
            self.draw_rect_line(kwargs['pygame'], kwargs['screen'], 400, 606, 68, 68, kwargs['color'].robrecht_blue, 6)
        kwargs['screen'].blit(self.rKeyImage, (400, 606))

        self.draw_text(kwargs['screen'], 'JUMP : ', 30, 720, 620, kwargs['color'].black, kwargs['pygame'])
        if self.space_active:
            self.draw_rect_line(kwargs['pygame'], kwargs['screen'], 800, 610, 240, 60, kwargs['color'].grey, kwargs['color'].robrecht_blue, 6)
        kwargs['screen'].blit(self.spaceBar, (800, 610))

    def draw_text(self, surf, text, size, x, y, color, pygame):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x,y)
        surf.blit(text_surface, text_rect)

    def draw_rect_line(self, pygame, screen, x, y, width, height, rect_color, line_color, line_thickness = 2):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.rect_color = rect_color
        self.line_color = line_color
        self.line_thickness = line_thickness

        pygame.draw.rect(screen, self.rect_color, (self.x, self.y, self.width, self.height))