class Bar:

    def __init__(self, pygame, settings,  **kwargs):
        self.barImage = pygame.image.load(
            settings.script_dir + "/minigames/minigame6/assets/Hoogspringen.png").convert_alpha()

        self.bar = self.barImage
        self.x = 800
        self.y = 300
        self.bar_rect = pygame.Rect(self.x, self.y, 92, 179)


    def draw(self, **kwargs):

        kwargs['screen'].blit(self.bar, self.bar_rect)
