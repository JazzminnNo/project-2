class Minigame4:

    green = (0, 255, 0)
    white = (255, 255, 255)
    black = (0, 0, 0)
    red = (255, 0, 0)

    def __init__(self, pygame, settings, screen, color, inputs, random, math, highscore, sound, **kwargs):
        self.screen = screen
        self.color = color
        self.inputs = inputs
        self.random = random
        self.settings = settings
        self.pygame = pygame
        self.math = math
        self.sound = sound
        self.hs = highscore

        self.font_name = pygame.font.match_font('verdana')
        self.font_small = pygame.font.Font(self.font_name, 12)
        self.font_bigger = pygame.font.Font(self.font_name, 16)
        self.font_text = pygame.font.Font(self.font_name, 30)
        self.font_biggest = pygame.font.Font(self.font_name, 150)
        self.bgImg = pygame.image.load(settings.script_dir + "/minigames/minigame4/assets/tribune_maybe.png").convert_alpha()
        self.OriArrowImg = pygame.image.load(settings.script_dir + "/minigames/minigame4/assets/arrow-md.png").convert_alpha()

        self.bg_stats = 0, 0, 1280, 320
        self.ground_stats = 0, 320, 1280, 400
        self.board_pos = 640, 360
        self.court_hard = (540, 720), (740, 720), (640, 360)
        self.court_medium = (540, 720), (620, 360), (660, 360), (740, 720)
        self.court_easy = (540, 720), (600, 360), (680, 360), (740, 720)

        self.wind_box = 30, 30, 80, 80
        self.wind_textbox = 30, 120, 130, 30
        self.arrows_textbox = 30, 595, 130, 30
        self.end_textbox = 175, 195, 900, 230
        self.score_textbox = 1130, 25, 130, 30

        self.mouse = self.pygame.mouse.get_pos()
        self.click = self.pygame.mouse.get_pressed()
        self.x_mouse = self.mouse[0]
        self.y_mouse = self.mouse[1]

    def draw_text(self, surf, text, size, x, y, color, pygame):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        surf.blit(text_surface, text_rect)

    def show_start_screen(self, screen, settings, pygame):
        image = self.pygame.Surface([settings.width, settings.height]).convert()
        image.set_alpha(200)
        image.fill((0, 0, 0))
        rect = image.get_rect()
        rect.center = (settings.width / 2, (settings.height / 2))
        screen.blit(image, rect)
        self.draw_text(screen, 'Archery!', 64, settings.width / 2, settings.height / 4, self.color.white, pygame)  # game splash/start screen
        self.draw_text(screen, 'Press Space to start the game', 22, settings.width / 2, settings.height / 2, self.color.white, pygame)  # game splash/start screen
        self.draw_text(screen, 'Highscore is: ' + str(self.highscore), 22, settings.width / 2, settings.height / 2 + 60, self.color.white, pygame)

    def draw_lined_rect(self, pygame, screen, x, y, width, height, rect_color, line_color, line_thickness=2):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.rect_color = rect_color
        self.line_color = line_color
        self.line_thickness = line_thickness

        pygame.draw.rect(screen, self.rect_color, (self.x, self.y, self.width, self.height))
        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x + self.width, self.y), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y + self.height), (self.x + self.width, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x + self.width, self.y), (self.x + self.width, self.y + self.height), self.line_thickness)

    def start(self, **kwargs):
        self.wind_x = self.random.randint(-10, 10) * 4
        self.wind_y = self.random.randint(-10, 10) * 4
        self.draw_arrow = False
        self.arrow_y = None
        self.arrow_x = None
        self.last_score = None
        self.shots_left = 10
        self.score = 0
        self.game_finished = False
        self.can_click = True
        self.show_score = False
        self.started = False
        self.play_sound = True

        try:
            self.difficulty = kwargs['selected_button']
        except:
            pass

        self.highscore = self.hs.load_highscore_archery(self.difficulty)

        if self.difficulty == 0:
            self.arrowImg = self.pygame.transform.scale(self.OriArrowImg, (90, 90))
            self.arHeight = 90
            self.gravity = 20
        if self.difficulty == 1:
            self.arrowImg = self.pygame.transform.scale(self.OriArrowImg, (60, 60))
            self.gravity = 30
            self.arHeight = 60
        if self.difficulty == 2:
            self.arrowImg = self.pygame.transform.scale(self.OriArrowImg, (30, 30))
            self.gravity = 40
            self.arHeight = 30

    def update(self, **kwargs):

        if self.started:
            if self.click[0] == 0:
                self.can_click = True

            self.mouse = self.pygame.mouse.get_pos()
            self.x_mouse = self.mouse[0]
            self.y_mouse = self.mouse[1]
            self.click = kwargs['pygame'].mouse.get_pressed()

            if self.click[0] == 1 and self.can_click and not self.game_finished:
                self.show_score = True
                self.arrow_x = self.x_mouse + self.wind_x
                self.arrow_y = self.y_mouse + self.wind_y + self.gravity
                self.shots_left -= 1
                self.can_click = False
                sqx = (self.arrow_x - 640) ** 2
                sqy = (self.arrow_y - 360) ** 2
                if self.difficulty == 0:
                    if self.math.sqrt(sqx + sqy) < 30:
                        self.last_score = 10
                        self.score += 10
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 60:
                        self.last_score = 8
                        self.score += 8
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 90:
                        self.last_score = 6
                        self.score += 6
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 120:
                        self.last_score = 4
                        self.score += 4
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 150:
                        self.last_score = 2
                        self.score += 2
                        self.draw_arrow = True
                    else:
                        self.draw_arrow = False
                        self.last_score = 0
                if self.difficulty == 1:
                    if self.math.sqrt(sqx + sqy) < 20:
                        self.last_score = 10
                        self.score += 10
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 40:
                        self.last_score = 8
                        self.score += 8
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 60:
                        self.last_score = 6
                        self.score += 6
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 80:
                        self.last_score = 4
                        self.score += 4
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 100:
                        self.last_score = 2
                        self.score += 2
                        self.draw_arrow = True
                    else:
                        self.draw_arrow = False
                        self.last_score = 0
                if self.difficulty == 2:
                    if self.math.sqrt(sqx + sqy) < 10:
                        self.last_score = 10
                        self.score += 10
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 20:
                        self.last_score = 8
                        self.score += 8
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 30:
                        self.last_score = 6
                        self.score += 6
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 40:
                        self.last_score = 4
                        self.score += 4
                        self.draw_arrow = True
                    elif self.math.sqrt(sqx + sqy) < 50:
                        self.last_score = 2
                        self.score += 2
                        self.draw_arrow = True
                    else:
                        self.draw_arrow = False
                        self.last_score = 0
                if self.last_score > 0:
                    self.sound.arrow_sound.play()
                if self.last_score == 0:
                    self.sound.arrow_miss.play()
                self.wind_x = self.random.randint(-10, 10) * 4
                self.wind_y = self.random.randint(-10, 10) * 4
                if self.shots_left == 0:
                    self.game_finished = True
            if self.game_finished and self.score > self.highscore:
                kwargs['menu'].score_screen_show(**kwargs, text="You broke the old record of " + str(self.highscore)+ " points!\n\n\nThe new highscore is " + str(self.score) + " points!")
                self.hs.save_highscore(self.score, 4, self.difficulty)
            if self.game_finished and self.score < self.highscore:
                kwargs['menu'].score_screen_show(**kwargs, text="Unfortunately, you did not break the record of " + str(
                    self.highscore) + " points.\n\n\nYour score was " + str(self.score) + " points.")

    def draw(self, **kwargs):
        k = self.pygame.key.get_pressed()
        if k[self.pygame.K_SPACE]:
            self.started = True
        self.screen.blit(self.bgImg, (0, 0))
        self.pygame.draw.rect(self.screen, self.green, self.ground_stats)

        self.court_hard = (540, 720), (740, 720), (640, 360)
        self.court_medium = (540, 720), (620, 360), (660, 360), (740, 720)
        self.court_easy = (540, 720), (600, 360), (680, 360), (740, 720)

        ###BOARD###
        if self.difficulty == 0:
            self.pygame.draw.polygon(self.screen, self.color.uc_brown, self.court_easy)
            self.pygame.draw.line(self.screen, self.black, (540, 720), (600, 360), 2)
            self.pygame.draw.line(self.screen, self.black, (680, 360), (740, 720), 2)
            self.pygame.draw.circle(self.screen, self.black, self.board_pos, 150)
            self.pygame.draw.circle(self.screen, self.white, self.board_pos, 120)
            self.pygame.draw.circle(self.screen, self.black, self.board_pos, 90)
            self.pygame.draw.circle(self.screen, self.white, self.board_pos, 60)
            self.pygame.draw.circle(self.screen, self.black, self.board_pos, 30)

        if self.difficulty == 1:
            self.pygame.draw.polygon(self.screen, self.color.uc_brown, self.court_medium)
            self.pygame.draw.line(self.screen, self.black, (540, 720), (620, 360), 2)
            self.pygame.draw.line(self.screen, self.black, (660, 360), (740, 720), 2)
            self.pygame.draw.circle(self.screen, self.black, self.board_pos, 100)
            self.pygame.draw.circle(self.screen, self.white, self.board_pos, 80)
            self.pygame.draw.circle(self.screen, self.black, self.board_pos, 60)
            self.pygame.draw.circle(self.screen, self.white, self.board_pos, 40)
            self.pygame.draw.circle(self.screen, self.black, self.board_pos, 20)

        if self.difficulty == 2:
            self.pygame.draw.polygon(self.screen, self.color.uc_brown, self.court_hard)
            self.pygame.draw.line(self.screen, self.black, (540, 720), (640, 360), 2)
            self.pygame.draw.line(self.screen, self.black, (640, 360), (740, 720), 2)
            self.pygame.draw.circle(self.screen, self.black, self.board_pos, 50)
            self.pygame.draw.circle(self.screen, self.white, self.board_pos, 40)
            self.pygame.draw.circle(self.screen, self.black, self.board_pos, 30)
            self.pygame.draw.circle(self.screen, self.white, self.board_pos, 20)
            self.pygame.draw.circle(self.screen, self.black, self.board_pos, 10)

        ###TEXT BOXES###
        self.draw_lined_rect(self.pygame, self.screen, 20, 20, 130, 180, self.color.light_grey, self.color.black)
        self.draw_lined_rect(self.pygame, self.screen, 45, 110, 80, 80, self.color.dark_gold, self.color.black)
        self.draw_lined_rect(self.pygame, self.screen, 35, 30, 100, 60, self.color.robrecht_blue, self.color.black)

        # ARROWS LEFT CONTAINER
        self.draw_lined_rect(self.pygame, self.screen, 20, 600, 350, 80, self.color.light_grey, self.color.black)
        self.draw_lined_rect(self.pygame, self.screen, 35, 610, 230, 60, self.color.robrecht_blue, self.color.black)
        self.draw_lined_rect(self.pygame, self.screen, 275, 610, 80, 60, self.color.dim_gray, self.color.black)
        # SCORE CONTAINER
        self.draw_lined_rect(self.pygame, self.screen, 965, 5, 295, 80, self.color.light_grey, self.color.black)
        self.draw_lined_rect(self.pygame, self.screen, 980, 15, 170, 60, self.color.black, self.color.black)
        self.draw_lined_rect(self.pygame, self.screen, 1160, 15, 85, 60, self.color.dark_gold, self.color.black)

        ###TEXT AND WIND###
        self.pygame.draw.line(self.screen, self.black, (85, 150), ((85 + self.wind_x), (150 + self.wind_y)), 2)
        self.pygame.draw.circle(self.screen, self.black, (85, 150), 3)

        score_text = self.font_text.render("SCORE ", True, self.white)
        self.screen.blit(score_text, (1000, 25))
        score_number = self.font_text.render(str(self.score), True, self.black)
        self.screen.blit(score_number, (1180, 25))
        shots_left_text = self.font_text.render("SHOTS LEFT", True, self.black)
        self.screen.blit(shots_left_text, (45, 619))
        number_left = self.font_text.render(str(self.shots_left), True, self.black)
        self.screen.blit(number_left, (300, 619))
        wind_text2 = self.font_text.render("WIND", True, self.black)
        self.screen.blit(wind_text2, (40, 39))

        ###DRAW SHOT ARROWS###
        if self.draw_arrow:
            self.screen.blit(self.arrowImg, (self.arrow_x, (self.arrow_y - self.arHeight)))

        if self.show_score:
            self.draw_lined_rect(self.pygame, self.screen, 540, 100, 200, 60, self.color.light_grey, self.color.black)
            last_score_text = self.font_text.render("" + str(self.last_score) + " POINTS", True, self.black)
            self.screen.blit(last_score_text, (555, 109))

        if self.game_finished:

            if self.play_sound and self.score > 75:
                self.sound.crowd_cheer_sound.play()
                self.pygame.mixer.fadeout(6000)
                self.play_sound = False
            if self.play_sound and self.score < 75:
                self.sound.crowd_boo_sound.play()
                self.pygame.mixer.fadeout(6000)
                self.play_sound = False

        if not self.started:
            self.show_start_screen(self.screen, self.settings, self.pygame)

