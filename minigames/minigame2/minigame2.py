class Minigame2:

    def __init__(self, pygame, screen, settings, assets, inputs, color, random, highscore, sound):
        self.screen = screen
        self.settings = settings
        self.pygame = pygame
        self.inputs = inputs
        self.color = color
        self.random = random
        self.assets = assets
        self.hs = highscore
        self.sound = sound

        self.screenWidth = settings.width
        self.screenHeight = settings.height

        self.bkgdImg = assets.bg
        self.hurdleImg = pygame.image.load(settings.script_dir +
                                           "/minigames/minigame2/assets/horde_korter.png").convert_alpha()
        self.hurdleImg = pygame.transform.scale(self.hurdleImg, (110, 150))
        self.finishImg = pygame.image.load(settings.script_dir +
                                           "/minigames/minigame2/assets/finishline.png").convert_alpha()
        self.finishImg = pygame.transform.scale(self.finishImg, (150, 300))

        self.leftkeyImg = assets.key_left
        self.leftkeyImg = pygame.transform.scale(self.leftkeyImg, (75, 75))
        self.rightkeyImg = assets.key_right
        self.rightkeyImg = pygame.transform.scale(self.rightkeyImg, (75, 75))
        self.spacebarImg = assets.spacebar

        self.bkgdX = 0
        self.groundY = 250

        self.hurdleAdditionalDistance = 2000
        self.distanceLastHurdleFinish = 4000

        self.fps = settings.fps
        self.font_name = pygame.font.match_font('verdana')
        self.font = pygame.font.Font(self.font_name, 35)
        self.fontBigger = self.pygame.font.Font(self.font_name, 100)

        self.crowd_anticipation = True

    class Mover:

        def __init__(self, x, y, width, height, img, sound):
            self.x = x
            self.y = y
            self.width = width
            self.height = height
            self.img = img
            self.jump_index = 0
            self.jump_levels = [-24.5, -23.5, -22.5, -21.5, -20.5, -19.5, -18.5, -17.5, -16.5, -15.5, -14.5, -13.5,
                                -12.5, -11.5, -10.5, -9.5, -8.5, -7.5, -6.5, -5.5, -4.5, -3.5, -2.5, -1.5, -0.5, 0.5,
                                1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5, 8.5, 9.5, 10.5, 11.5, 12.5, 13.5, 14.5, 15.5, 16.5,
                                17.5, 18.5, 19.5, 20.5, 21.5, 22.5, 23.5, 24.5]
            self.jumping = False
            self.speed_index = 0
            self.speed_levels = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18]
            self.speed = self.speed_levels[self.speed_index]

            self.isHitting = False

            self.sound = sound

        def jump(self, pygame, inputs, ground_y):
            if inputs.isset(pygame.K_SPACE) and self.jumping is False:
                self.jumping = True
                self.started = True
                self.sound.jump_sound2.play()

            if self.jumping:
                self.y += self.jump_levels[self.jump_index]
                self.jump_index += 1
                if self.jump_index >= len(self.jump_levels) - 1:
                    self.jump_index = len(self.jump_levels) - 1
                if self.y > ground_y:
                    self.y = ground_y
                    self.sound.slime_jump.play()
                    self.jumping = False
                    self.jump_index = 0

        def modify_speed(self, pygame, inputs):
            if self.isHitting is False:
                if inputs.down(pygame.K_RIGHT):
                    self.speed = self.speed_levels[self.speed_index]
                    self.speed_index += 1
                    if self.speed_index >= len(self.speed_levels) - 1:
                        self.speed_index = len(self.speed_levels) - 1

                if inputs.down(pygame.K_LEFT):
                    self.speed = self.speed_levels[self.speed_index]
                    self.speed_index -= 3
                    if self.speed_index <= 0:
                        self.speed_index = 0

        def has_collided(self, hurdle, mover):
            hurdle.x, hurdle.y = hurdle.coordinates()
            if self.x + self.width > hurdle.x:
                if self.x < hurdle.x:
                    if self.y + self.height > hurdle.y:
                        self.isHitting = True
                        mover.speed_index = 2
                        mover.speed = mover.speed_levels[mover.speed_index]
                        return True
            self.isHitting = False
            return False

        def update(self, pygame, inputs, ground_y):
            self.jump(pygame, inputs, ground_y)
            self.modify_speed(pygame, inputs)

        def draw(self, screen):
            screen.blit(self.img, (self.x, self.y))

    class Hurdle:

        def __init__(self, x, y, width, height, img):
            self.x = x
            self.y = y
            self.width = width
            self.height = height
            self.img = img

        def coordinates(self):
            return self.x, self.y

        def draw(self, screen):
            screen.blit(self.img, (self.x, self.y))

    class Finish:

        def __init__(self, x, y, img):
            self.x = x
            self.y = y
            self.img = img

        def is_finished(self, mover):
            if self.x < mover.x:
                return True
            return False

        def update(self, mover):
            if self.is_finished(mover):
                self.x = self.x - 10
            else:
                self.x = self.x - mover.speed
                self.is_finished(mover)

        def draw(self, screen):
            screen.blit(self.img, (self.x, self.y))

    def setup_hurdles(self, y):

        if self.difficulty == 0:
            hurdles = []
            for i in range(0, 15):
                hurdles.append(self.Hurdle(((i * self.random.randint(950, 1000)) + self.hurdleAdditionalDistance), y, self.hurdleImg.get_rect().width, self.hurdleImg.get_rect().height, self.hurdleImg))
            return hurdles

        elif self.difficulty == 1:
            hurdles = []
            for i in range(0, 25):
                hurdles.append(self.Hurdle((i * self.random.randint(1000, 1200) + self.hurdleAdditionalDistance), y, self.hurdleImg.get_rect().width,
                                           self.hurdleImg.get_rect().height, self.hurdleImg))
            return hurdles

        else:
            hurdles = []
            for i in range(0, 30):
                hurdles.append(self.Hurdle((i * self.random.randint(900, 1050) + self.hurdleAdditionalDistance), y, self.hurdleImg.get_rect().width,
                                           self.hurdleImg.get_rect().height, self.hurdleImg))
            return hurdles

    def draw_hurdles(self, screen, all_hurdles, mover):
        for i in range(len(all_hurdles)):
            all_hurdles[i].draw(screen)
            if not self.settings.paused:
                self.mover.has_collided(all_hurdles[i], mover)
                all_hurdles[i].x -= mover.speed

    def show_countdown(self):
        text = self.fontBigger.render(str(self.totale_sec), True, (255, 255, 255))
        self.draw_lined_rect(self.pygame, self.screen, 0,  60, self.settings.width, 130, self.color.black, self.color.grey, 8)
        self.screen.blit(text, (self.settings.width / 2, 60))

    def show_timer(self):
        self.totale_seconden = self.fps_count // self.fps
        minuten = self.totale_seconden // 60
        seconden = self.totale_seconden % 60
        frame_per_seconde = self.fps_count % 60
        string = "{0:02}:{1:02}:{2:02}".format(minuten, seconden, frame_per_seconde)
        self.draw_text(self.screen, string, 25, 1170, 29, self.color.black, self.pygame)

    def show_start_screen(self, screen, settings, pygame):
        image = self.pygame.Surface([settings.width, settings.height]).convert()
        image.set_alpha(200)  # , pygame.SRCALPHA, 32)
        image.fill((0, 0, 0))
        rect = image.get_rect()
        rect.center = (settings.width / 2, (settings.height / 2))
        screen.blit(image, rect)
        self.draw_text(screen, 'Hurdles!', 64, settings.width / 2, settings.height / 4, self.color.white, pygame)  # game splash/start screen
        self.draw_text(screen, 'Press Space to start the game', 22, settings.width / 2, settings.height / 2, self.color.white, pygame)  # game splash/start screen
        self.draw_text(screen, 'The highscore to beat is: ' + self.convert_hs_to_time(), 22, settings.width / 2, settings.height / 2 + 60, self.color.white, pygame)

    def convert_hs_to_time(self):
        totale_sec = self.highscore // self.fps
        minuten = totale_sec // 60
        seconden = totale_sec % 60
        frame_per_seconde = self.highscore % 60
        string = "{0:02}:{1:02}:{2:02}".format(minuten, seconden, frame_per_seconde)
        return string

    def return_score(self, has_finished):
        if has_finished:
            if self.totale_seconden >= 60:
                return (self.fps_count // self.fps) // 60, (self.fps_count // self.fps) % 60, self.fps_count % 60
            else:
                return (self.fps_count // self.fps) % 60, self.fps_count % 60

    def draw_text(self, surf, text, size, x, y, color, pygame):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.midtop = (x, y)
        surf.blit(text_surface, text_rect)

    def draw_lined_rect(self, pygame, screen, x, y, width, height, rect_color, line_color, line_thickness=2):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.rect_color = rect_color
        self.line_color = line_color
        self.line_thickness = line_thickness

        pygame.draw.rect(screen, self.rect_color, (self.x, self.y, self.width, self.height))
        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x + self.width, self.y), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y + self.height), (self.x + self.width, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x + self.width, self.y), (self.x + self.width, self.y + self.height), self.line_thickness)

    def draw_hud(self, mover):
        # TIME CONTAINER
        self.draw_lined_rect(self.pygame, self.screen, 965, 5, 295, 80, self.color.light_grey, self.color.black)
        # TIME
        self.draw_lined_rect(self.pygame, self.screen, 980, 15, 110, 60, self.color.black, self.color.black)
        # TIMER
        self.draw_lined_rect(self.pygame, self.screen, 1100, 15, 140, 60, self.color.dark_gold, self.color.black)

        # SPEED CONTAINER
        self.draw_lined_rect(self.pygame, self.screen, 20, 600, 350, 80, self.color.light_grey, self.color.black)
        # SPEED
        self.draw_lined_rect(self.pygame, self.screen, 35, 610, 120, 60, self.color.green, self.color.black)
        # SPEED BAR CONTAINER
        self.draw_lined_rect(self.pygame, self.screen, 170, 610, 180, 60, self.color.dim_gray, self.color.black)
        # SPEED BAR
        if mover.speed > 0:
            if mover.speed < 7:
                self.draw_lined_rect(self.pygame, self.screen, 170, 610, mover.speed * 10, 60, self.color.green, self.color.black)
            elif mover.speed > 16:
                self.draw_lined_rect(self.pygame, self.screen, 170, 610, mover.speed * 10, 60, self.color.red, self.color.black)
            else:
                self.draw_lined_rect(self.pygame, self.screen, 170, 610, mover.speed * 10, 60, self.color.orange, self.color.black)

        # SPEED BAR BORDERS 1
        self.draw_lined_rect(self.pygame, self.screen, 170, 610, 180, 10, self.color.black, self.color.black)
        # SPEED BAR BORDERS 2
        self.draw_lined_rect(self.pygame, self.screen, 170, 660, 180, 10, self.color.black, self.color.black)

        # KEYS CONTAINER
        self.draw_lined_rect(self.pygame, self.screen, 450, 600, 800, 80, self.color.light_grey, self.color.black)
        # MOVE
        self.draw_lined_rect(self.pygame, self.screen, 465, 610, 120, 60, self.color.robrecht_blue, self.color.black)
        # LEFT ARROW
        self.draw_lined_rect(self.pygame, self.screen, 612, 605, 70, 70, self.color.grey, self.inputLeftKeyBorderColor, 6)
        self.screen.blit(self.leftkeyImg, (610, 605))

        # RIGHT ARROW
        self.draw_lined_rect(self.pygame, self.screen, 700, 605, 70, 70, self.color.grey, self.inputRightKeyBorderColor, 6)
        self.screen.blit(self.rightkeyImg, (700, 605))

        # JUMP
        self.draw_lined_rect(self.pygame, self.screen, 850, 610, 120, 60, self.color.robrecht_blue, self.color.black)
        # JUMP BUTTON
        self.draw_lined_rect(self.pygame, self.screen, 1000, 610, 240, 60, self.color.grey, self.inputSpaceKeyBorderColor, 6)
        self.screen.blit(self.spacebarImg, (1000, 610))

        # TIME TEXT
        self.draw_text(self.screen, 'TIME', 30, 1035, 25, self.color.white, self.pygame)

        # SPEED TEXT
        self.draw_text(self.screen, 'SPEED', 30, 95, 620, self.color.black, self.pygame)

        # MOVE TEXT
        self.draw_text(self.screen, 'Move:', 30, 525, 620, self.color.black, self.pygame)

        # JUMP TEXT
        self.draw_text(self.screen, 'Jump:', 30, 910, 620, self.color.black, self.pygame)
        # SPACE TEXT
        # self.draw_text(self.screen, 'SPACE', 30, 1165, 620, self.color.black, self.pygame)

    def start(self, **kwargs):
        self.moverImg = self.assets.blob3

        self.started = False
        self.fps_count = 0
        self.totale_seconden = 0  # seconden van de timer
        self.totale_sec = 0
        self.countdown = 3
        self.countdown_done = False
        self.fps_count_resetToZero = False

        try:
            self.difficulty = kwargs['selected_button']
        except:
            pass

        self.highscore = self.hs.load_highscore_horde(self.difficulty)
        
        self.mover = self.Mover(100, self.groundY, self.moverImg.get_rect().width,
                                self.moverImg.get_rect().height, self.moverImg, self.sound)
        self.all_hurdles = self.setup_hurdles(self.groundY + 25)

        self.allHurdleX = []
        for i in range(len(self.all_hurdles)):
            self.allHurdleX.append(self.all_hurdles[i].x)
        # print(self.allHurdleX)
        self.maxHurdleX = max(self.allHurdleX)
        #p rint('max hurdle x: ', self.maxHurdleX)
        self.finish = self.Finish(self.maxHurdleX + self.distanceLastHurdleFinish, self.groundY, self.finishImg)
        #p rint('finish x: ', self.finish.x)
        self.inputKeysDefaultColor = self.color.grey
        self.inputLeftKeyBorderColor = self.inputKeysDefaultColor
        self.inputRightKeyBorderColor = self.inputKeysDefaultColor
        self.inputSpaceKeyBorderColor = self.inputKeysDefaultColor

        self.moverFinished = False
        self.endTime = 0
        self.endTimeString = ''

        # SOUNDS CONTROL
        self.crowd_response_play = True

    def update(self, **kwargs):
        if self.inputs.down(self.pygame.K_SPACE):
            self.started = True

        if self.inputs.down(self.pygame.K_LEFT):
            self.inputLeftKeyBorderColor = self.color.blue
        else:
            self.inputLeftKeyBorderColor = self.inputKeysDefaultColor

        if self.inputs.down(self.pygame.K_RIGHT):
            self.inputRightKeyBorderColor = self.color.blue
        else:
            self.inputRightKeyBorderColor = self.inputKeysDefaultColor

        if self.inputs.down(self.pygame.K_SPACE):
            self.inputSpaceKeyBorderColor = self.color.blue
        else:
            self.inputSpaceKeyBorderColor = self.inputKeysDefaultColor

        if self.started:
            if self.moverFinished is False:
                self.fps_count += 1
                self.totale_sec = self.countdown - (self.fps_count // self.fps)

            if self.countdown_done is True:
                if self.moverFinished is False:
                        self.mover.update(kwargs['pygame'], kwargs['inputs'], self.groundY)
            if self.moverFinished is True:
                self.mover.y = self.groundY
                self.endTime = (self.return_score(self.moverFinished))
                if len(self.endTime) == 3:
                    self.endTimeString = "{0:02}:{1:02}:{2:02}".format(self.endTime[0], self.endTime[1], self.endTime[2])
                else:
                    self.endTimeString = "{0:02}.{1:02} seconds".format(self.endTime[0], self.endTime[1])

                if self.fps_count < self.highscore:
                    kwargs['menu'].score_screen_show(**kwargs, text='Time:   ' + self.endTimeString + "\n\nA new highscore is achieved! It's now:  " + self.endTimeString)
                if self.fps_count >= self.highscore:
                    kwargs['menu'].score_screen_show(**kwargs,
                                                     text='Time:   ' + self.endTimeString + "\n\nSuch a shame! The highscore to beat is: " + self.convert_hs_to_time())

            self.finish.update(self.mover)
            self.moverFinished = self.finish.is_finished(self.mover)

    def draw(self, **kwargs):
        self.rel_x = self.bkgdX % self.bkgdImg.get_rect().width
        self.screen.blit(self.bkgdImg, (self.rel_x - self.bkgdImg.get_rect().width, 0))

        if self.rel_x < self.screenWidth:
            self.screen.blit(self.bkgdImg, (self.rel_x, 0))

        if not self.settings.paused:
            if self.moverFinished is False:
                self.bkgdX -= self.mover.speed
            else:
                self.bkgdX -= 1.5

        self.finish.draw(self.screen)
        self.mover.draw(self.screen)

        if not self.moverFinished:
            self.draw_hurdles(self.screen, self.all_hurdles, self.mover)

        if self.totale_sec > 0 and not self.countdown_done:
            self.show_countdown()
            self.sound.race_countdown.play()

        if self.totale_sec == 0 and not self.countdown_done and self.started:
            self.draw_lined_rect(self.pygame, self.screen, 0, 60, self.settings.width, 130, self.color.black,
                                 self.color.dark_gold, 8)
            self.draw_text(self.screen, 'Start', 100, self.settings.width / 2, 60, (255, 255, 255), self.pygame)

        if -1 < self.totale_sec < 0:
            self.totale_sec = 0

        if not self.fps_count_resetToZero and self.totale_sec == -1:
            self.fps_count = 0
            self.fps_count_resetToZero = True
            self.countdown_done = True

        if self.countdown_done and not self.moverFinished:
            self.draw_hud(self.mover)
            self.show_timer()

        if self.moverFinished:
            if self.fps_count < self.highscore:
                '''self.draw_lined_rect(self.pygame, self.screen, 0, 60, self.settings.width, 130, self.color.black, self.color.dark_gold, 8)
                self.draw_text(self.screen, 'Time:   ' + self.endTimeString, 100, self.screenWidth / 2, 60, self.color.white, self.pygame)
                self.draw_lined_rect(self.pygame, self.screen, 0, 500, self.settings.width, 30, self.color.black, self.color.dark_gold, 5)
                self.draw_text(self.screen, "A new highscore is achieved! It's now:  " + self.endTimeString, 20, self.settings.width / 2, 500, self.color.white, self.pygame)
                self.hs.save_highscore(self.fps_count, 2, self.difficulty)'''
                if self.crowd_response_play is True:
                    self.sound.partyhorn.play()
                    self.sound.crowd_cheer_sound.play()
                    self.pygame.mixer.fadeout(18000)
                    self.crowd_response_play = False
            else:
                '''self.draw_lined_rect(self.pygame, self.screen, 0, 60, self.settings.width, 130, self.color.black, self.color.grey, 8)
                self.draw_text(self.screen, 'Time:   ' + self.endTimeString, 100, self.screenWidth / 2, 60, self.color.white, self.pygame)
                self.draw_lined_rect(self.pygame, self.screen, 0, 500, self.settings.width, 30, self.color.black, self.color.grey, 5)
                self.draw_text(self.screen, 'Such a shame! The highscore to beat is: ' + self.convert_hs_to_time(), 20, self.settings.width / 2, 500, self.color.white, self.pygame)'''
                if self.crowd_response_play is True:
                    self.sound.cue_scratch.play()
                    self.sound.crowd_boo_sound.play()
                    self.pygame.mixer.fadeout(18000)
                    self.crowd_response_play = False

        if not self.started:
            self.show_start_screen(self.screen, self.settings, self.pygame)
            self.mover.speed = 0
            if self.crowd_anticipation is True:
                self.sound.start_crowd_sound.play()
                self.crowd_anticipation = False
                self.pygame.mixer.fadeout(16000)

        if self.started is True:
            self.pygame.mixer.fadeout(6000)
