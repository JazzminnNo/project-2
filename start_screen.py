class StartScreen:

    def __init__(self):
        self.intro_text = ["Javelin", "Hurdles", "Trampoline", "Archery", "Skiïng", "High Jumping"]
        self.minigame = None
        self.instructions = "Instructions:\n\n\tWait for the AI to throw his spear first, then try to beat his\n\t score!\n\n\tHold RIGHT key to gain speed, and use the UP and DOWN\n\t keys to change the angle of the spear.\n\n\tPress SPACE to throw the spear.",\
                            "Instructions:\n\n\tReach the finish as fast as possible!\n\n\tTap LEFT and RIGHT arrow keys to change speed levels.\n\n\tUse SPACEBAR to jump over obstacles.\n\n\t\tHINT: you can change speed while airborne.", \
                            "Instructions:\n\n\tPress the letters(WASD) as fast as you can on your keyboard\n\taccording to the order shown on the screen(Top to Bottom)\n\tto score points while jumping.\n\n\t" \
                            + "Press SPACE to make Blob jump from the trampoline and to\n\tmake the letters appear.", \
                            "Instructions:\n\n\tCLICK on the LEFT mouse button to fire an arrow.\n\n\tTry to hit the center of the target for the biggest score.\n\n\tBe wary of the wind!", \
                            "Instructions:\n\n\tFinish the race as fast as you can. Touch all flags\n\tto get the highest score.\n\n\tUse LEFT and RIGHT arrow keys" \
                            + " to move while\n\tracing. Hold DOWN key to speed up.",\
                            "Instructions:\n\n\tJump over the crossbar and land on the mat\n\tto win the game.\n\n\t" \
                            "Press RIGHT key to move forward\n\t Hold SPACEBAR to jump"

    def draw(self, **kwargs):
        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 310, 90, 660, 300, kwargs['color'].white, kwargs['color'].robrecht_blue)
        self.render_text_list(kwargs['screen'], self.wrap_text(self.intro_text[self.minigame] + "\n\n" + self.instructions[self.minigame], kwargs['font'], 620), kwargs['font'], kwargs['color'].black, 330, 110)

    def setMinigame(self, minigame):
        self.minigame = minigame

    def wrap_text(self, text, font, width):
        text_lines = text.replace('\t', '    ').split('\n')
        if width is None or width == 0:
            return text_lines

        wrapped_lines = []
        for line in text_lines:
            line = line.rstrip() + ' '
            if line == ' ':
                wrapped_lines.append(line)
                continue

            start = len(line) - len(line.lstrip())
            start = line.index(' ', start)
            while start + 1 < len(line):
                next = line.index(' ', start + 1)
                if font.size(line[:next])[0] <= width:
                    start = next
                else:
                    wrapped_lines.append(line[:start])
                    line = line[start + 1:]
                    start = line.index(' ')
            line = line[:-1]
            if line:
                wrapped_lines.append(line)
        return wrapped_lines

    def render_text_list(self, screen, lines, font, color, start_x, start_y):
        rendered = [font.render(line, True, color).convert_alpha()
                    for line in lines]

        line_height = font.get_linesize()
        tops = [int(round(i * line_height)) for i in range(len(rendered))]

        for y, line in zip(tops, rendered):
            screen.blit(line, (start_x, start_y + y))

    def draw_lined_rect(self, pygame, screen, x, y, width, height, rect_color, line_color, line_thickness=3):
        pygame.draw.rect(screen, rect_color, (x, y, width, height))
        pygame.draw.line(screen, line_color, (x, y), (x + width, y), line_thickness)
        pygame.draw.line(screen, line_color, (x, y), (x, y + height), line_thickness)
        pygame.draw.line(screen, line_color, (x, y + height), (x + width, y + height), line_thickness)
        pygame.draw.line(screen, line_color, (x + width, y), (x + width, y + height), line_thickness)
