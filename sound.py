class Sound:

    def __init__(self, os, pygame):

        self.snd_dir = os.path.join(os.path.dirname(__file__), 'Sounds')

        # CHARACTER SOUNDS
        self.jump_sound = pygame.mixer.Sound(os.path.join(self.snd_dir, 'qubodup-cfork-ccby3-jump.ogg'))
        self.jump_sound.set_volume(0.3)
        self.jump_sound2 = pygame.mixer.Sound(os.path.join(self.snd_dir, 'robrecht_jump.wav'))
        self.squish_landing = pygame.mixer.Sound(os.path.join(self.snd_dir, 'robrecht_squish_landing.wav'))
        self.slime_jump = pygame.mixer.Sound(os.path.join(self.snd_dir, 'slime_jump.wav'))

        # CROWD SOUNDS
        self.crowd_cheer_sound = pygame.mixer.Sound(os.path.join(self.snd_dir, 'ccs.wav'))
        self.crowd_boo_sound = pygame.mixer.Sound(os.path.join(self.snd_dir, 'crowd_boo.wav'))
        self.start_crowd_sound = pygame.mixer.Sound(os.path.join(self.snd_dir, 'startscreen_crowd.wav'))

        # GAME EFFECTS SOUNDS
        self.arrow_sound = pygame.mixer.Sound(os.path.join(self.snd_dir, 'arrow_sound.wav'))
        self.arrow_miss = pygame.mixer.Sound(os.path.join(self.snd_dir, 'arrow_miss.wav'))
        self.woosh = pygame.mixer.Sound(os.path.join(self.snd_dir, 'robrecht_woosh.wav'))
        self.partyhorn = pygame.mixer.Sound(os.path.join(self.snd_dir, 'partyhorn.wav'))
        self.race_countdown = pygame.mixer.Sound(os.path.join(self.snd_dir, 'race_countdown.ogg'))
        self.cue_scratch = pygame.mixer.Sound(os.path.join(self.snd_dir, 'cue_scratch.wav'))
        self.key_hit_sound = pygame.mixer.Sound(os.path.join(self.snd_dir, 'Menu2A.wav'))
        self.ski_sound = pygame.mixer.Sound(os.path.join(self.snd_dir, 'Skisound3.wav'))
        self.jeej = pygame.mixer.Sound(os.path.join(self.snd_dir, 'jeej.ogg'))
        self.ohoh = pygame.mixer.Sound(os.path.join(self.snd_dir, 'oh-oh.wav'))


        # VOLUME CONTROL
        self.key_hit_sound.set_volume(0.3)
        self.race_countdown.set_volume(0.150)