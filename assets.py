class Assets:

    def __init__(self, pygame, settings):
        self.blobs = ["/assets/blob.png", "/assets/blob_blauw.png", "/assets/blob_geel.png", "/assets/blob_groen.png",
                      "/assets/blob_lichtblauw.png","/assets/blob_rood.png","/assets/blob_roze.png" ]
        self.kawaiiblobs = ["/assets/blob2.png", "/assets/KawaiiBlob_blauw.png", "/assets/KawaiiBlob_geel.png", "/assets/KawaiiBlob_groen.png",
                            "/assets/KawaiiBlob_lichtblauw.png", "/assets/KawaiiBlob_rood.png", "/assets/KawaiiBlob_roze.png" ]
        self.ath_blobs = ["/assets/ath_blob.png", "/assets/ath_blob_blauw.png", "/assets/ath_blob_geel.png", "/assets/ath_blob_roze.png",
                          "/assets/ath_blob_groen.png", "/assets/ath_blob_lichtblauw.png", "/assets/ath_blob_rood.png"]
        self.henk_blobs = ["/assets/henkblob.png", "/assets/henkblob_groen.png", "/assets/henkblob_blauw.png",
                           "/assets/henkblob_geel.png",
                           "/assets/henkblob_lichtblauw.png", "/assets/henkblob_rood.png", "/assets/henkblob_roze.png"]

        self.blob = pygame.image.load(settings.script_dir + "/assets/blob.png").convert_alpha()
        self.blob2 = pygame.image.load(settings.script_dir + "/assets/blob2.png").convert_alpha()
        self.blob3 = pygame.image.load(settings.script_dir + "/assets/ath_blob.png").convert_alpha()
        self.fakkelblob = pygame.image.load(settings.script_dir + "/assets/fakkelblob.png").convert_alpha()
        self.henkblob = pygame.image.load(settings.script_dir + "/assets/henkblob.png").convert_alpha()
        self.bg = pygame.image.load(settings.script_dir + "/assets/bg.png").convert()
        self.bg = pygame.transform.scale(self.bg, (1280, 720))
        self.bg_empty = pygame.image.load(settings.script_dir + "/assets/bg_empty.png").convert()
        self.bg_empty = pygame.transform.scale(self.bg_empty, (1280, 720))
        self.bg_dark = pygame.image.load(settings.script_dir + "/assets/bg_dark.png").convert()
        self.bg_dark = pygame.transform.scale(self.bg_dark, (1280, 720))
        self.logo = pygame.image.load(settings.script_dir + "/assets/logo.png").convert_alpha()
        self.key_left = pygame.image.load(settings.script_dir + "/assets/key_left.png").convert_alpha()
        self.key_right = pygame.image.load(settings.script_dir + "/assets/key_right.png").convert_alpha()
        self.key_up = pygame.image.load(settings.script_dir + "/assets/key_up.png").convert_alpha()
        self.key_down = pygame.image.load(settings.script_dir + "/assets/key_down.png").convert_alpha()
        self.spacebar = pygame.image.load(settings.script_dir + "/assets/space_bar2.png").convert_alpha()
        self.wasd = pygame.image.load(settings.script_dir + "/assets/wasd.png").convert_alpha()
        self.wasd = pygame.transform.scale(self.wasd, (50, 50))
        self.key_w = pygame.image.load(settings.script_dir + "/assets/key_w.png").convert_alpha()
        self.key_w = pygame.transform.scale(self.key_w, (50, 50))
        self.key_a = pygame.image.load(settings.script_dir + "/assets/key_a.png").convert_alpha()
        self.key_a = pygame.transform.scale(self.key_a, (50, 50))
        self.key_s = pygame.image.load(settings.script_dir + "/assets/key_s.png").convert_alpha()
        self.key_s = pygame.transform.scale(self.key_s, (50, 50))
        self.key_d = pygame.image.load(settings.script_dir + "/assets/key_d.png").convert_alpha()
        self.key_d = pygame.transform.scale(self.key_d, (50, 50))

    def random_blob(self, pygame, settings, random):
        self.randomBlob = random.choice(self.blobs)
        self.randomKawaii = random.choice(self.kawaiiblobs)
        self.randomAth = random.choice(self.ath_blobs)
        self.randomHenk = random.choice(self.henk_blobs)
        self.blob = pygame.image.load(settings.script_dir + self.randomBlob).convert_alpha()
        self.blob2 = pygame.image.load(settings.script_dir + self.randomKawaii).convert_alpha()
        self.blob3 = pygame.image.load(settings.script_dir + self.randomAth).convert_alpha()
        self.henkblob = pygame.image.load(settings.script_dir + self.randomHenk).convert_alpha()


