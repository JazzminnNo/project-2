class Highscores:

    def __init__(self, os):
        self.hs_easy_file = ["highscores/highscore_speer_easy.txt", "highscores/highscore_horde_easy.txt",
                        "highscores/highscore_trampoline_easy.txt",
                        "highscores/highscore_archery_easy.txt", "highscores/highscore_skiing_easy.txt",
                        "highscores/highscore_hoogspringen_easy.txt"]
        self.hs_medium_file = ["highscores/highscore_speer_medium.txt", "highscores/highscore_horde_medium.txt",
                          "highscores/highscore_trampoline_medium.txt",
                          "highscores/highscore_archery_medium.txt", "highscores/highscore_skiing_medium.txt",
                          "highscores/highscore_hoogspringen_medium.txt"]
        self.hs_hard_file = ["highscores/highscore_speer_hard.txt", "highscores/highscore_horde_hard.txt",
                        "highscores/highscore_trampoline_hard.txt",
                        "highscores/highscore_archery_hard.txt", "highscores/highscore_skiing_hard.txt",
                        "highscores/highscore_hoogspringen_hard.txt"]
        self.os = os

    def load_highscore_speer(self, dif):
        self.dir = self.os.path.dirname(__file__)
        self.cijfer = dif
        if self.cijfer == 0:
            self.hs_file = self.hs_easy_file
        if self.cijfer == 1:
            self.hs_file = self.hs_medium_file
        if self.cijfer == 2:
            self.hs_file = self.hs_hard_file
        with open(self.os.path.join(self.dir, self.hs_file[0]), 'r+') as f:
            try:
                self.highscore = int(f.read())
            except:
                self.highscore = 0
        return self.highscore

    def load_highscore_horde(self, dif):
        self.dir = self.os.path.dirname(__file__)
        self.cijfer = dif
        if self.cijfer == 0:
            self.hs_file = self.hs_easy_file
        if self.cijfer == 1:
            self.hs_file = self.hs_medium_file
        if self.cijfer == 2:
            self.hs_file = self.hs_hard_file
        with open(self.os.path.join(self.dir, self.hs_file[1]), 'r+') as f:
            try:
                self.highscore = int(f.read())
            except:
                self.highscore = 0
        return self.highscore

    def load_highscore_trampoline(self, dif):
        self.dir = self.os.path.dirname(__file__)
        self.cijfer = int(dif)
        if self.cijfer == 0:
            self.hs_file = self.hs_easy_file
        if self.cijfer == 1:
            self.hs_file = self.hs_medium_file
        if self.cijfer == 2:
            self.hs_file = self.hs_hard_file
        with open(self.os.path.join(self.dir, self.hs_file[2]), 'r+') as f:
            try:
                self.highscore = int(f.read())
            except:
                self.highscore = 0
        return self.highscore

    def load_highscore_archery(self, dif):
        self.dir = self.os.path.dirname(__file__)
        self.cijfer = dif
        if self.cijfer == 0:
            self.hs_file = self.hs_easy_file
        if self.cijfer == 1:
            self.hs_file = self.hs_medium_file
        if self.cijfer == 2:
            self.hs_file = self.hs_hard_file
        with open(self.os.path.join(self.dir, self.hs_file[3]), 'r+') as f:
            try:
                self.highscore = int(f.read())
            except:
                self.highscore = 0
        return self.highscore

    def load_highscore_skiing(self, dif):
        self.dir = self.os.path.dirname(__file__)
        self.cijfer = dif
        if self.cijfer == 0:
            self.hs_file = self.hs_easy_file
        if self.cijfer == 1:
            self.hs_file = self.hs_medium_file
        if self.cijfer == 2:
            self.hs_file = self.hs_hard_file
        with open(self.os.path.join(self.dir, self.hs_file[4]), 'r+') as f:
            try:
                self.highscore = int(f.read())
            except:
                self.highscore = 0
        return self.highscore

    def load_highscore_hoogspringen(self, dif):
        self.dir = self.os.path.dirname(__file__)
        self.cijfer = dif
        if self.cijfer == 0:
            self.hs_file = self.hs_easy_file
        if self.cijfer == 1:
            self.hs_file = self.hs_medium_file
        if self.cijfer == 2:
            self.hs_file = self.hs_hard_file
        with open(self.os.path.join(self.dir, self.hs_file[5]), 'r+') as f:
            try:
                self.highscore = int(f.read())
            except:
                self.highscore = 0
        return self.highscore

   # def get_score(self, pname, score, minigamenumber):
       # self.dir = self.os.path.dirname(__file__)
       # self.index_position = minigamenumber - 1
        #self.score = pname + ": " + str(score)
       # with open(self.os.join(self.dir, self.hs_file[self.index_position]), 'w') as f:
         #   f.write(score + "\n")
    def open_highscore_hs_screen(self, dif, mininr):
        self.dir = self.os.path.dirname(__file__)
        self.cijfer = dif
        if self.cijfer == 0:
            self.hs_file = self.hs_easy_file
        if self.cijfer == 1:
            self.hs_file = self.hs_medium_file
        if self.cijfer == 2:
            self.hs_file = self.hs_hard_file
        with open(self.os.path.join(self.dir, self.hs_file[mininr]), 'r+') as f:
            try:
                self.highscore = int(f.read())
            except:
                self.highscore = 0
        return self.highscore

    def save_highscore(self, score, minigamenumber, dif):
        self.index_position = minigamenumber - 1
        self.cijfer = dif
        if self.cijfer == 0:
            self.hs_file = self.hs_easy_file
        if self.cijfer == 1:
            self.hs_file = self.hs_medium_file
        if self.cijfer == 2:
            self.hs_file = self.hs_hard_file
        with open(self.os.path.join(self.dir, self.hs_file[self.index_position]), 'r+') as f:
            f.write(str(score))

    def high_score_screen(self):
        pass