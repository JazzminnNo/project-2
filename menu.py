import button
import highscore_screen
import start_screen
import credits
import score_screen
from enum import Enum


class MenuState(Enum):
    MAIN = 0
    QUICK_PLAY_MENU = 1
    QUICK_PLAY_START_SCREEN = 2
    QUICK_PLAY = 3
    HIGHSCORE_MENU = 4
    HIGHSCORE_SCREEN = 5
    CREDIT_SCREEN = 6
    SCORE_SCREEN = 7

class Menu:

    selected_button = 0
    selected__minigame = None
    current_buttons = []
    start_screen = start_screen.StartScreen()
    highscore_screen = highscore_screen.HighscoreScreen()
    credit_screen = credits.CreditsScreen()
    score_screen = score_screen.ScoreScreen()

    def __init__(self, **kwargs):
        self.main_buttons = [None, (4, 0), 200, 50, 'Play', self.quick_play_menu, kwargs['settings'], kwargs['color'], kwargs['font']], \
                            [None, (4, 1), 200, 50, 'High Scores', self.highscore_menu, kwargs['settings'], kwargs['color'], kwargs['font']], \
                            [None, (4, 2), 200, 50, 'Credits', self.credit_display, kwargs['settings'], kwargs['color'], kwargs['font']], \
                            [None, (4, 3), 200, 50, 'Exit', self.button_exit, kwargs['settings'], kwargs['color'], kwargs['font']]

        self.quick_play_buttons = [None, (7, 0), 200, 50, 'Javelin', self.quick_play_minigame, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                  [None, (7, 1), 200, 50, 'Hurdles', self.quick_play_minigame, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                  [None, (7, 2), 200, 50, 'Trampoline', self.quick_play_minigame, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                  [None, (7, 3), 200, 50, 'Archery', self.quick_play_minigame, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                  [None, (7, 4), 200, 50, 'Skiïng', self.quick_play_minigame, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                  [None, (7, 5), 200, 50, 'High Jumping', self.quick_play_minigame, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                  [None, (7, 6), 200, 50, 'Back', self.main_menu, kwargs['settings'], kwargs['color'], kwargs['font']]

        self.pause_buttons = [None, (2, 0), 200, 50, 'Resume', self.close_pause_menu, kwargs['settings'], kwargs['color'], kwargs['font']], \
                             [None, (2, 1), 200, 50, 'Main Menu', self.main_menu, kwargs['settings'], kwargs['color'], kwargs['font']]

        self.start_screen_buttons = [(3, 0), (6, 4), 200, 50, 'Easy', self.start_game, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                    [(3, 1), (6, 4), 200, 50, 'Medium', self.start_game, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                    [(3, 2), (6, 4), 200, 50, 'Hard', self.start_game, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                    [None, (6, 5), 200, 50, 'Back', self.quick_play_menu, kwargs['settings'], kwargs['color'], kwargs['font']]

        self.highscore_screen_buttons = [None, (7, 0), 200, 50, 'Javelin', self.highscore_display, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                        [None, (7, 1), 200, 50, 'Hurdles', self.highscore_display, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                        [None, (7, 2), 200, 50, 'Trampoline', self.highscore_display, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                        [None, (7, 3), 200, 50, 'Archery', self.highscore_display, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                        [None, (7, 4), 200, 50, 'Skiïng', self.highscore_display, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                        [None, (7, 5), 200, 50, 'High Jumping', self.highscore_display, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                        [None, (7, 6), 200, 50, 'Back', self.main_menu, kwargs['settings'], kwargs['color'], kwargs['font']]

        self.highscore_screen2_buttons = [None, (7, 6), 200, 50, 'Back', self.highscore_menu, kwargs['settings'], kwargs['color'], kwargs['font']]

        self.credit_screen_buttons = [None, (7, 6), 200, 50, 'Back', self.main_menu, kwargs['settings'], kwargs['color'], kwargs['font']]

        self.score_screen_buttons = [(3, 0), (6, 4), 200, 50, 'Play Again', self.start_game2, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                        [(3, 1), (6, 4), 200, 50, 'Change Difficulty', self.quick_play_minigame2, kwargs['settings'], kwargs['color'], kwargs['font']], \
                                        [(3, 2), (6, 4), 200, 50, 'Main Menu', self.main_menu, kwargs['settings'], kwargs['color'], kwargs['font']]

        self.main_menu(settings=kwargs['settings'])

    def update(self, **kwargs):
        for x, current_button in enumerate(self.current_buttons):
            current_button.update(inputs=kwargs['inputs'], settings=kwargs['settings'], minigames=kwargs['minigames'], selected_button=x, assets=kwargs['assets'], pygame=kwargs['pygame'], random=kwargs['random'], color=kwargs['color'], font=kwargs['font'])

        if kwargs['settings'].current_minigame is None or kwargs['settings'].paused or self.state is MenuState.SCORE_SCREEN:
            if kwargs['inputs'].down(kwargs['pygame'].K_UP) and self.state is not MenuState.SCORE_SCREEN:
                self.current_buttons[self.selected_button].selected = False

                if self.state is MenuState.QUICK_PLAY_START_SCREEN:
                    if self.selected_button is not len(self.current_buttons) - 1:
                        self.selected_button = len(self.current_buttons) - 1
                    else:
                        self.selected_button = 0
                else:
                    if self.selected_button == 0:
                        self.selected_button = len(self.current_buttons) - 1
                    else:
                        self.selected_button -= 1
                self.current_buttons[self.selected_button].selected = True

            if kwargs['inputs'].down(kwargs['pygame'].K_DOWN) and self.state is not MenuState.SCORE_SCREEN:
                self.current_buttons[self.selected_button].selected = False

                if self.state is MenuState.QUICK_PLAY_START_SCREEN:
                    if self.selected_button is not len(self.current_buttons) - 1:
                        self.selected_button = len(self.current_buttons) - 1
                    else:
                        self.selected_button = 0
                else:
                    if self.selected_button == len(self.current_buttons) - 1:
                        self.selected_button = 0
                    else:
                        self.selected_button += 1
                self.current_buttons[self.selected_button].selected = True

            if self.state is MenuState.QUICK_PLAY_START_SCREEN:
                if kwargs['inputs'].down(kwargs['pygame'].K_LEFT) and self.selected_button is not len(self.current_buttons) - 1:
                    self.current_buttons[self.selected_button].selected = False

                    if self.selected_button == 0:
                        self.selected_button = len(self.current_buttons) - 2
                    else:
                        self.selected_button -= 1

                    self.current_buttons[self.selected_button].selected = True

                if kwargs['inputs'].down(kwargs['pygame'].K_RIGHT) and self.selected_button is not len(self.current_buttons) - 1:
                    self.current_buttons[self.selected_button].selected = False

                    if self.selected_button == len(self.current_buttons) - 2:
                        self.selected_button = 0
                    else:
                        self.selected_button += 1

                    self.current_buttons[self.selected_button].selected = True

            if self.state is MenuState.SCORE_SCREEN:
                if kwargs['inputs'].down(kwargs['pygame'].K_LEFT):
                    self.current_buttons[self.selected_button].selected = False

                    if self.selected_button == 0:
                        self.selected_button = len(self.current_buttons) - 1
                    else:
                        self.selected_button -= 1

                    self.current_buttons[self.selected_button].selected = True

                if kwargs['inputs'].down(kwargs['pygame'].K_RIGHT):
                    self.current_buttons[self.selected_button].selected = False

                    if self.selected_button == len(self.current_buttons) - 1:
                        self.selected_button = 0
                    else:
                        self.selected_button += 1

                    self.current_buttons[self.selected_button].selected = True

            if kwargs['inputs'].down(kwargs['pygame'].K_RETURN):
                self.current_buttons[self.selected_button].activate(settings=kwargs['settings'], minigames=kwargs['minigames'], selected_button=self.selected_button, assets=kwargs['assets'], pygame=kwargs['pygame'], random=kwargs['random'], color=kwargs['color'], font=kwargs['font'])

            if kwargs['settings'].paused and kwargs['inputs'].down(kwargs['pygame'].K_ESCAPE):
                kwargs['settings'].paused = False
                del self.current_buttons[:]
        else:
            if kwargs['inputs'].down(kwargs['pygame'].K_ESCAPE) and not self.state is MenuState.SCORE_SCREEN:
                kwargs['settings'].paused = True
                del self.current_buttons[:]

                self.create_buttons(self.pause_buttons)

                self.selected_button = 0
                self.current_buttons[0].selected = True

    def draw(self, **kwargs):
        if kwargs['settings'].paused and not self.state is MenuState.SCORE_SCREEN:
            kwargs['pygame'].draw.rect(kwargs['screen'], kwargs['color'].white, (kwargs['settings'].width / 2 - 150, kwargs['settings'].height / 2 - (len(self.pause_buttons) * 80 + 50) / 2, 300, len(self.pause_buttons) * 80 + 50))
            kwargs['pygame'].draw.rect(kwargs['screen'], kwargs['color'].black, (kwargs['settings'].width / 2 - 145, kwargs['settings'].height / 2 - (len(self.pause_buttons) * 80 + 40) / 2, 290, len(self.pause_buttons) * 80 + 40))

        for current_button in self.current_buttons:
            current_button.draw(pygame=kwargs['pygame'], screen=kwargs['screen'])

        if self.state is MenuState.QUICK_PLAY_START_SCREEN:
            self.start_screen.draw(pygame=kwargs['pygame'], screen=kwargs['screen'], color=kwargs['color'], font=kwargs['font'].fps)

        if self.state is MenuState.HIGHSCORE_SCREEN:
            self.highscore_screen.draw(pygame=kwargs['pygame'], screen=kwargs['screen'], color=kwargs['color'], highscore=kwargs['highscore'])

        if self.state is MenuState.CREDIT_SCREEN:
            self.credit_screen.draw(pygame=kwargs['pygame'], screen=kwargs['screen'], color=kwargs['color'])

        if self.state is MenuState.SCORE_SCREEN:
            self.score_screen.draw(pygame=kwargs['pygame'], screen=kwargs['screen'], color=kwargs['color'], font=kwargs['font'].fps)

    def create_buttons(self, buttons):
        for x in range(len(buttons)):
            if len(buttons[x]) == 2:
                self.current_buttons.append(button.Button(*buttons[x][0], **buttons[x][1][0]))
            else:
                self.current_buttons.append(button.Button(*buttons[x]))

    def main_menu(self, **kwargs):
        self.state = MenuState.MAIN

        kwargs['settings'].current_minigame = None
        kwargs['settings'].paused = False
        del self.current_buttons[:]

        self.create_buttons(self.main_buttons)

        self.selected_button = 0
        self.current_buttons[0].selected = True

    def quick_play_menu(self, **kwargs):
        self.state = MenuState.QUICK_PLAY_MENU

        del self.current_buttons[:]

        self.create_buttons(self.quick_play_buttons)

        self.selected_button = 0
        self.current_buttons[0].selected = True

    def button_exit(self, **kwargs):
        kwargs['settings'].running = False

    def quick_play_minigame(self, **kwargs):
        self.state = MenuState.QUICK_PLAY_START_SCREEN

        del self.current_buttons[:]
        self.selected__minigame = kwargs['selected_button']
        self.start_screen.setMinigame(self.selected__minigame)

        self.create_buttons(self.start_screen_buttons)

        self.selected_button = 0
        self.current_buttons[0].selected = True

    def quick_play_minigame2(self, **kwargs):
        self.state = MenuState.QUICK_PLAY_START_SCREEN

        kwargs['settings'].current_minigame = None
        kwargs['settings'].paused = False

        del self.current_buttons[:]
        self.start_screen.setMinigame(self.selected__minigame)

        self.create_buttons(self.start_screen_buttons)

        self.selected_button = 0
        self.current_buttons[0].selected = True

    def start_game(self, **kwargs):
        self.state = MenuState.QUICK_PLAY
        kwargs['settings'].paused = False

        kwargs['assets'].random_blob(kwargs['pygame'], kwargs['settings'], kwargs['random'])

        del self.current_buttons[:]

        kwargs['settings'].current_minigame = self.selected__minigame
        kwargs['minigames'][self.selected__minigame].start(**kwargs)

    def start_game2(self, **kwargs):
        self.state = MenuState.QUICK_PLAY
        kwargs['settings'].paused = False

        kwargs['assets'].random_blob(kwargs['pygame'], kwargs['settings'], kwargs['random'])

        del self.current_buttons[:]

        kwargs.pop('selected_button', None)

        kwargs['settings'].current_minigame = self.selected__minigame
        kwargs['minigames'][self.selected__minigame].start(**kwargs)

    def highscore_menu(self, **kwargs):
        self.state = MenuState.HIGHSCORE_MENU

        del self.current_buttons[:]

        self.create_buttons(self.highscore_screen_buttons)

        self.selected_button = 0
        self.current_buttons[0].selected = True

    def highscore_display(self, **kwargs):
        self.state = MenuState.HIGHSCORE_SCREEN

        del self.current_buttons[:]

        self.selected__minigame = kwargs['selected_button']
        self.highscore_screen.setMinigame(self.selected__minigame)
        self.create_buttons([self.highscore_screen2_buttons])

        self.selected_button = 0
        self.current_buttons[0].selected = True

    def credit_display(self, **kwargs):
        self.state = MenuState.CREDIT_SCREEN

        del self.current_buttons[:]

        self.create_buttons([self.credit_screen_buttons])

        self.selected_button = 0
        self.current_buttons[0].selected = True

    def score_screen_show(self, **kwargs):
        self.state = MenuState.SCORE_SCREEN
        kwargs['settings'].paused = True

        self.score_screen.set_text(text=kwargs['text'])

        del self.current_buttons[:]

        self.create_buttons(self.score_screen_buttons)

        self.selected_button = 0
        self.current_buttons[0].selected = True

    def close_pause_menu(self, **kwargs):
        del self.current_buttons[:]
        kwargs['settings'].paused = False
