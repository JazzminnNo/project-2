class MenuScreen:

    def __init__(self, pygame, settings, assets, color):
        self.bg = pygame.image.load(settings.script_dir + "/assets/blob_veld_metlucht_full.png").convert_alpha()
        self.bg_rect = self.bg.get_rect()

       #self.bg_rect.bottom = settings.height + 125
        self.logo = pygame.image.load(settings.script_dir + "/assets/logo.png").convert_alpha()
        self.logo = pygame.transform.scale(self.logo, (280, 280))
        self.logo_rect = self.logo.get_rect()
        self.logo_rect.center = 635, 100
        self.settings = settings
        self.blobs = []

        self.image = assets.fakkelblob
        # os.path.join(img_folder, "Blob2.png")
        self.image.set_colorkey(color.black)
        self.image = pygame.transform.scale(self.image, (230, 230))
        # de rect that surrounds the image/the sprite
        self.rect = self.image.get_rect()
        self.rect.centerx = -230
        self.rect.bottom =  557

        self.s = pygame.Surface((240, 330))  # the size of your rect
        self.s.set_alpha(200)  # alpha level
        self.s.fill(color.black)  # this fills the entire surface

        self.vel = 0
        self.x = 0
        self.r = 0

    def update_blobs(self, **kwargs):
        self.vel = 6
        self.r = 6

        if self.rect.left > 850:
            self.vel = 0
            self.bg_rect.x -= self.r
            #self.x = 6

        self.rect.x += self.vel

    def draw_screen(self, **kwargs):
        self.rel_x = self.bg_rect.x % self.bg.get_rect().width
        kwargs['screen'].blit(self.bg, (self.rel_x - self.bg.get_rect().width, 0))
        if self.rel_x < kwargs['settings'].width:
            kwargs['screen'].blit(self.bg, (self.rel_x, 0))

        kwargs['screen'].blit(self.logo, (0,0))
        kwargs['screen'].blit(self.logo, (1000, 0))

        kwargs['screen'].blit(kwargs['assets'].fakkelblob, self.rect)
        #kwargs['screen'].blit(self.s, (520, 195))  # (0,0) are the top-left coordinates



