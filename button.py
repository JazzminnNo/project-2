class Button:

    active = False
    selected = False
    clicked = False

    def __init__(self, *args, **kwargs):
        self.x = args[0]
        self.y = args[1]
        self.width = args[2]
        self.height = args[3]
        self.label = args[4]
        self.callback = args[5]

        if 'color_n' in kwargs:
            self.color_button = kwargs['color_n']
            self.color_button_normal = kwargs['color_n']
        else:
            self.color_button = args[7].robrecht_blue
            self.color_button_normal = args[7].robrecht_blue

        if 'color_a' in kwargs:
            self.color_button_active = kwargs['color_a']
        else:
            self.color_button_active = args[7].grey

        if 'color_t' in kwargs:
            self.color_text = kwargs['color_t']
        else:
            self.color_text = args[7].white

        if 'font' in kwargs:
            self.font = kwargs['font']
        else:
            self.font = args[8].fps

        if self.x is None:
            self.x = args[6].width / 2 - self.width / 2

        if isinstance(self.x, tuple):
            self.x = (args[6].width / 2) - (self.width / 2) - ((self.x[0] - 1) * 230 / 2) + (self.x[1] * 230)

        if isinstance(self.y, tuple):
            self.y = (args[6].height / 2) - (self.height / 2) - ((self.y[0] - 1) * 80 / 2) + (self.y[1] * 80)

    def update(self, **kwargs):
        if self.x <= kwargs['inputs'].mouse_pos[0] <= self.x + self.width and self.y <= kwargs['inputs'].mouse_pos[1] <= self.y + self.height:
            if not self.active:
                self.active = True
                self.color_button = self.color_button_active
            if kwargs['inputs'].down("MB1"):
                self.clicked = True
            if kwargs['inputs'].up("MB1") and self.clicked:
                self.activate(**kwargs)
        elif self.active:
            self.active = False
            self.color_button = self.color_button_normal

    def draw(self, **kwargs):
        kwargs['pygame'].draw.rect(kwargs['screen'], self.color_button, (self.x, self.y, self.width, self.height))
        kwargs['screen'].blit(self.font.render(self.label, True, self.color_text), (self.x + self.width / 2 - self.font.size(self.label)[0] / 2, self.y + self.height / 2 - self.font.size(self.label)[1] / 2))

        if self.selected:
            kwargs['pygame'].draw.rect(kwargs['screen'], self.color_button, (self.x - 12, self.y - 12, 25, 5))
            kwargs['pygame'].draw.rect(kwargs['screen'], self.color_button, (self.x - 12, self.y - 12, 5, 25))
            kwargs['pygame'].draw.rect(kwargs['screen'], self.color_button, (self.x + self.width - 13, self.y - 12, 25, 5))
            kwargs['pygame'].draw.rect(kwargs['screen'], self.color_button, (self.x + self.width + 7, self.y - 12, 5, 25))
            kwargs['pygame'].draw.rect(kwargs['screen'], self.color_button, (self.x - 12, self.y + self.height - 13, 5, 25))
            kwargs['pygame'].draw.rect(kwargs['screen'], self.color_button, (self.x - 12, self.y + self.height + 7, 25, 5))
            kwargs['pygame'].draw.rect(kwargs['screen'], self.color_button, (self.x + self.width + 7, self.y + self.height - 13, 5, 25))
            kwargs['pygame'].draw.rect(kwargs['screen'], self.color_button, (self.x + self.width - 13, self.y + self.height + 7, 25, 5))

    def activate(self, **kwargs):
        self.callback(**kwargs)
