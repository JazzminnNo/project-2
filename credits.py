class CreditsScreen:

    def __init__(self):
        pass

    def draw_text(self, surf, text, size, x, y, color, pygame):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.center = (x, y)
        surf.blit(text_surface, text_rect)

    def rect_with_text(self, pygame, screen, surf, pos_x, pos_y, width, height, rect_color, line_color,  text, text_size, text_color, alpha):
        self.draw_lined_rect(pygame, screen, pos_x, pos_y, width, height, rect_color, line_color, alpha, line_thickness=2)
        self.draw_text(surf, text, text_size, pos_x + 320, pos_y + 20, text_color, pygame)

    def draw_lined_rect(self, pygame, screen, x, y, width, height, rect_color, line_color, alpha=255, line_thickness=2):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.rect_color = rect_color
        self.line_color = line_color
        self.line_thickness = line_thickness

        s = pygame.Surface((width, height))  # the size of your rect
        s.set_alpha(alpha)  # alpha level
        s.fill(rect_color)  # this fills the entire surface
        screen.blit(s, (x, y))  # (0,0) are the top-left coordinates

        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x + self.width, self.y), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y + self.height), (self.x + self.width, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x + self.width, self.y), (self.x + self.width, self.y + self.height), self.line_thickness)

    def draw(self, **kwargs):

        self.rect_with_text(kwargs['pygame'], kwargs['screen'], kwargs['screen'], 320, 100, 640, 400, kwargs['color'].white,
                                kwargs['color'].black, "Thank you for playing Blobolympics 2018!", 25, kwargs['color'].black, 255)
        self.draw_text(kwargs['screen'], "This game was made for a project at the Hogeschool Rotterdam.", 18, 640, 200, kwargs['color'].black, kwargs['pygame'])
        self.draw_text(kwargs['screen'],
                       "We hope you enjoyed playing our game!",
                       18, 640, 225, kwargs['color'].black, kwargs['pygame'])
        self.draw_text(kwargs['screen'],
                       "Made by:",
                       14, 640, 290, kwargs['color'].black, kwargs['pygame'])
        self.draw_text(kwargs['screen'],
                       "Ahmad Silevani 0945922@hr.nl",
                       14, 640, 310, kwargs['color'].black, kwargs['pygame'])
        self.draw_text(kwargs['screen'],
                       "Henk Hemme 0855974@hr.nl",
                       14, 640, 325, kwargs['color'].black, kwargs['pygame'])
        self.draw_text(kwargs['screen'],
                       "Joessi Moorman 0957787@hr.nl",
                       14, 640, 340, kwargs['color'].black, kwargs['pygame'])
        self.draw_text(kwargs['screen'],
                       "Nofit Kartoredjo 0942332@hr.nl",
                       14, 640, 355, kwargs['color'].black, kwargs['pygame'])
        self.draw_text(kwargs['screen'],
                       "Robrecht Cornelis 0863028@hr.nl",
                       14, 640, 370, kwargs['color'].black, kwargs['pygame'])
        self.draw_text(kwargs['screen'],
                       "Tanja Nguyen 0931179@hr.nl",
                       14, 640, 385, kwargs['color'].black, kwargs['pygame'])