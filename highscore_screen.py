class HighscoreScreen:


    def __init__(self):

        self.intro_text = ["Javelin", "Hurdles", "Trampoline", "Archery", "Skiïng", "High Jumping"]

        self.dif = ["Easy", "Medium", "Hard"]

    def setMinigame(self, minigame_index):
        self.minigame = minigame_index
        if self.minigame == 1 or self.minigame == 4:
            self.convert = True
        elif self.minigame != 1 or self.minigame != 4:
            self.convert = False

    def print_highscore(self, highscore, i):
        self.highscore = highscore.open_highscore_hs_screen(i, self.minigame)
        if self.convert:
            string = self.convert_hs_to_time()
            return string
        elif not self.convert:
            string = str(self.highscore)
            return string

    def draw(self, **kwargs):
        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 120, 82, 1036, 450, kwargs['color'].storyboard, kwargs['color'].black, 200)
        self.rect_with_text(kwargs['pygame'], kwargs['screen'], kwargs['screen'], 138, 100, 1000, 70,
                            kwargs['color'].white,
                            kwargs['color'].black, self.intro_text[self.minigame], 50, kwargs['color'].black, 255)
        for i in range(len(self.dif)):
            self.rect_with_text(kwargs['pygame'], kwargs['screen'], kwargs['screen'], 138 + (i*400), 200, 200, 50, kwargs['color'].white,
                                kwargs['color'].black, self.dif[i], 20, kwargs['color'].black, 255)
            self.rect_with_text(kwargs['pygame'], kwargs['screen'], kwargs['screen'], 138 + (i*400), 300, 200, 50,
                                kwargs['color'].white, kwargs['color'].black, self.print_highscore(kwargs['highscore'], i) , 20, kwargs['color'].black, 255)

    def rect_with_text(self, pygame, screen, surf, pos_x, pos_y, width, height, rect_color, line_color,  text, text_size, text_color, alpha):
        self.draw_lined_rect(pygame, screen, pos_x, pos_y, width, height, rect_color, line_color, alpha, line_thickness=2)
        self.draw_text(surf, text, text_size, pos_x + (width/2), pos_y + (height/2), text_color, pygame)

    def draw_lined_rect(self, pygame, screen, x, y, width, height, rect_color, line_color, alpha=255, line_thickness=2):
        self.x = x
        self.y = y
        self.width = width
        self.height = height
        self.rect_color = rect_color
        self.line_color = line_color
        self.line_thickness = line_thickness

        s = pygame.Surface((width, height))  # the size of your rect
        s.set_alpha(alpha)  # alpha level
        s.fill(rect_color)  # this fills the entire surface
        screen.blit(s, (x, y))  # (0,0) are the top-left coordinates

        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x + self.width, self.y), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y), (self.x, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x, self.y + self.height), (self.x + self.width, self.y + self.height), self.line_thickness)
        pygame.draw.line(screen, self.line_color, (self.x + self.width, self.y), (self.x + self.width, self.y + self.height), self.line_thickness)

    def draw_text(self, surf, text, size, x, y, color, pygame):
        font_name = pygame.font.match_font('Verdana')
        font = pygame.font.Font(font_name, size)
        text_surface = font.render(text, True, color)
        text_rect = text_surface.get_rect()
        text_rect.center = (x, y)
        surf.blit(text_surface, text_rect)

    def convert_hs_to_time(self):
        totale_sec = self.highscore // 60
        minuten = totale_sec // 60
        seconden = totale_sec % 60
        frame_per_seconde = self.highscore % 60
        string = "{0:02}:{1:02}:{2:02}".format(minuten, seconden, frame_per_seconde)
        return string


