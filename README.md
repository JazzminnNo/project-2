# Project 2 : Creating​ ​a​ ​video​ ​game
In dit project maakt elk teamlid een eigen mini game. De mini-games worden uiteindelijk samengevoegd tot één spel. Hiervoor is de samenhang
van de mini games belangrijk. Om het uiteindelijke product te kunnen leveren wordt er een goede samenwerking, 
waaronder het nakomen van afspraken, verwacht.


## Contributors

Tanja Nguyen 0931179@hr.nl </br>
Nofit Kartoredjo 0942332@hr.nl </br>
Robrecht Cornelis 0863028@hr.nl </br>
Joessi Moorman 0957787@hr.nl</br>
Ahmad Silevani 0945922@hr.nl </br>
Henk Hemme 0855974@hr.nl
