class ScoreScreen:

    def __init__(self):
        self.text = ""

    def draw(self, **kwargs):
        self.draw_lined_rect(kwargs['pygame'], kwargs['screen'], 310, 120, 660, 300, kwargs['color'].white, kwargs['color'].robrecht_blue)
        self.render_text_list(kwargs['screen'], self.wrap_text(self.text, kwargs['font'], 620), kwargs['font'], kwargs['color'].black, 330, 140)

    def set_text(self, **kwargs):
        self.text = kwargs['text']

    def wrap_text(self, text, font, width):
        text_lines = text.replace('\t', '    ').split('\n')
        if width is None or width == 0:
            return text_lines

        wrapped_lines = []
        for line in text_lines:
            line = line.rstrip() + ' '
            if line == ' ':
                wrapped_lines.append(line)
                continue

            start = len(line) - len(line.lstrip())
            start = line.index(' ', start)
            while start + 1 < len(line):
                next = line.index(' ', start + 1)
                if font.size(line[:next])[0] <= width:
                    start = next
                else:
                    wrapped_lines.append(line[:start])
                    line = line[start + 1:]
                    start = line.index(' ')
            line = line[:-1]
            if line:
                wrapped_lines.append(line)
        return wrapped_lines

    def render_text_list(self, screen, lines, font, color, start_x, start_y):
        rendered = [font.render(line, True, color).convert_alpha()
                    for line in lines]

        line_height = font.get_linesize()
        tops = [int(round(i * line_height)) for i in range(len(rendered))]

        for y, line in zip(tops, rendered):
            screen.blit(line, (start_x, start_y + y))

    def draw_lined_rect(self, pygame, screen, x, y, width, height, rect_color, line_color, line_thickness=3):
        pygame.draw.rect(screen, rect_color, (x, y, width, height))
        pygame.draw.line(screen, line_color, (x, y), (x + width, y), line_thickness)
        pygame.draw.line(screen, line_color, (x, y), (x, y + height), line_thickness)
        pygame.draw.line(screen, line_color, (x, y + height), (x + width, y + height), line_thickness)
        pygame.draw.line(screen, line_color, (x + width, y), (x + width, y + height), line_thickness)
