class Inputs(object):

    DOWN = 1
    MOTION = 2
    UP = 3
    btn = {1: "MB1", 3: "MB3"}

    events = {}
    button_state = ()
    mouse_pos = ()

    def add_event(self, event, pygame):
        if event.type == pygame.KEYDOWN:
            self.events.update({event.key: [event, self.DOWN]})
        elif event.type == pygame.KEYUP:
            self.events.update({event.key: [event, self.UP]})
        elif event.type == pygame.MOUSEBUTTONDOWN:
            if self.btn.get(event.button):
                self.events.update({self.btn[event.button]: [event, self.DOWN]})
        elif event.type == pygame.MOUSEBUTTONUP:
            if self.btn.get(event.button):
                self.events.update({self.btn[event.button]: [event, self.UP]})
        elif event.type == pygame.MOUSEMOTION:
            self.events.update({pygame.MOUSEMOTION: [event, self.MOTION]})
        elif event.type == pygame.QUIT:
            self.events.update({pygame.QUIT: [event, True]})

    def add_events(self, events, pygame):
        for event in events:
            self.add_event(event, pygame)

    def get(self, name):
        return self.events.get(name)

    def event(self, name):
        event = self.get(name)

        if event is not None:
            event = event[0]

        return event

    def stat(self, name):
        event = self.get(name)

        if event is not None:
            status = event[1]
        else:
            status = None

        return status

    def isset(self, name):
        return name in self.events

    def down(self, name):
        return self.stat(name) == self.DOWN

    def motion(self, name):
        return self.stat(name) == self.MOTION

    def up(self, name):
        return self.stat(name) == self.UP

    def update_mouse(self, buttons=(0, 0, 0), pos=(0, 0)):
        self.mouse_pos = pos
        self.button_state = buttons

    def update(self, pygame):
        flag = []

        for i in self.events:
            if self.events[i][0].type == pygame.KEYDOWN:
                self.events[i][1] = self.MOTION
            elif self.events[i][0].type == pygame.KEYUP:
                flag.append(i)
            elif i == pygame.MOUSEMOTION:
                flag.append(i)
            elif i in self.btn.values():
                if i.startswith("MB"):
                    if self.down(i):
                        self.events[i][1] = self.MOTION
                    if self.up(i):
                        flag.append(i)

        for i in flag:
            del self.events[i]
